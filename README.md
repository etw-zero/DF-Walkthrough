## PeridexisErrant的矮人要塞入门教程
[![Build Status](https://travis-ci.org/PeridexisErrant/DF-Walkthrough.svg)]
(https://travis-ci.org/PeridexisErrant/DF-Walkthrough)
[![Docs Status](https://readthedocs.org/projects/df-walkthrough/badge)]
(https://df-walkthrough.rtfd.org)

Resources for people who want to play Dwarf Fortress, but can't yet.

给那些想要去体验矮人要塞但是被劝退的人提供一些资源。

* A simple step-by-step walkthrough to play along with.
* Tutorials on specific topics for new players.
* Lots of external links and advice.

* 一个简单的按照游戏流程的攻略，可以边看边玩。
* 为萌新们准备了一些特定方面的教程。
* 很多的参考链接与建议。

[Check it out here!](https://df-walkthrough.readthedocs.org)
[教程全文看这里！](https://df-walkthrough.readthedocs.org)

If you're interested in contributing, I'd love a hand! The main priority is
updating the walkthrough and moving non-core content to tutorials.  More
tutorials would also be great!

如果你对贡献教程内容感兴趣的话，我是十分乐意的！目前的主要优先事项是更新当前的游戏攻略，然后删掉教程里的非核心内容。当然如果有更多的教程的话，那就更好了！

当前的任务：粗译所有章节，目前正在翻译第二章。