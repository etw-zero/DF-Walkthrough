################################
PeridexisErrant的矮人要塞入门教程
################################

The learning curve for Dwarf Fortress has been described as a spiked
overhang - but many of us have fought our way up, and have more fun
than any other game around.  This walkthrough, along with the
:forums:`DF Starter Pack <126076>`, are my attempts to dangle a rope
down, as the community first did for me years ago.

矮人要塞的学习曲线曾被描述为一条垂直陡峭的线——但是我们很多人都在努力拼搏，同样也拥有远多于其他游戏的快乐与收获。这个入门教程，配备着 :forums:`DF Starter Pack <126076>` ，是我将一根绳子吊下来的一次尝试，正如我们的游戏社区在几年前对我做的一样。

.. _index:

入门流程
===============
Dwarf Fortress is a rare game:  every time you start a new game, an
entire world is created anew - everything from geology and weather,
to the patterns of trade in wheat and wars between jealous kings.
The fact that there is no script, no standard storyline that every
player goes through, makes a traditional game walkthrough impossible.

矮人要塞是一个十分难能可贵的游戏：每次你开始了新的旅程，这个世界将是完完全全重新生成的——无论从地理和气候，到交易小麦队伍的格局和贪婪国王之间的战火纷飞。事实是，每一次玩家所进行的游戏，都是没有剧本的，没有一个标准的故事大纲。让一个传统的游戏攻略难以进行下去。

Instead this is more like a tutorial campaign in a strategy game -
right down to playing on the same map, if you download the suggested
package.  You're free to follow along exactly, but also encouraged to
experiment (and maybe back up your save occasionally).

但是，如果你游玩的是相同的地图，下载一个推荐的游戏包，那么这就更像是策略游戏里的教程战役。你完全可以确切地按照教程的每一步进行下去，当然我们也十分鼓励玩家们去自己探索（以及或许可以考虑偶尔备份一下存档什么的）。

Remember as you play that no matter how skilled you become, without a
winning condition your fortress will inevitably fail - then pick yourself
up,  reflect on what you learnt, and recite the motto:  **Losing is Fun!**

记住了，无论你对这个游戏是有多么擅长，但是这个游戏没有一个确切的胜利条件，因为你的要塞最终都会以不同的方式，不可避免地消逝——然后重振旗鼓，反思你所学到的东西，然后默念游戏的座右铭： **失败很有趣！**


.. toctree::
   :numbered:
   :maxdepth: 1
   :glob:

   chapters/*


教程
=========
The walkthrough covers the topics you'll need in every fortress, in just
enough detail to get you started.  After that, it's your decision what
to focus on - the options are endless, from beekeeping to soap-making to
digging too deep, or learning to make and install mods.

这个入门教程包含了你所需要关心的所有要塞相关的方方面面，对于你的入门而言也会有足够多的细节。在此之后，你可以考虑接下来会着重研究哪些方面——这些内容可是无穷无尽的，从养蜂，到肥皂工艺制作，再到向更深处挖掘，或是学习如何制作与安装模组。

Each tutorial is a short, self-contained introduction to a topic which is
likely to be of interest to players who have just finished the walkthrough.
And if you're most interested in somthing not listed here,
:wiki:`the wiki <>` and the Bay12 Forums will be your best friends.

每一篇教程都有一个简短的主题介绍，面向于那些已经看完全部的入门流程后对其他方面感兴趣的玩家。如果你所感兴趣的内容并没有在这里列出来， :wiki:`the wiki <>` 和 Bay12 论坛将会是你最好的朋友。


.. toctree::
   :maxdepth: 1
   :glob:

   tutorials/*


大师课程
===========
A masterclass is a tutorial on an advanced topic, aimed at experienced
players.  You'll be taken through some of the most difficult challenges
DF offers, and see how an expert does things.

大师班是一些更加高级的话题，旨在为更加有经验的玩家们提供帮助。你需要通过一些矮人要塞中最为困难的挑战，并且来查看专业人士是如何解决问题的。


.. toctree::
   :maxdepth: 1
   :glob:

   masterclass/*


其他信息
=================

.. toctree::
   :maxdepth: 1
   :glob:

   LICENSE
   misc/*


关于
=====
The source text and images are `hosted on Github
<https://github.com/PeridexisErrant/DF-Walkthrough>`_ and the finished site is
published immediately to `readthedocs.org <https://df-walkthrough.rtfd.org>`_
every time a change is pushed.

原始文本于图像都 `存档在Github中
<https://github.com/PeridexisErrant/DF-Walkthrough>`_ ，同时每次更改被推送后，完成的页面将会立即更新到 `readthedocs.org <https://df-walkthrough.rtfd.org>`_ 。（中文译文存档在`Gitee
<https://gitee.com/etw-zero/DF-Walkthrough>`_ ，尚未生成最终页面。）

This project was inspired by `TinyPirate's DF tutorials
<http://afteractionreporter.com/dwarf-fortress-tutorials/>`_, which taught
me to play DF.  The walkthrough started as an update of these tutorials.
Other content is original or (for some tutorials and most masterclasses)
links back to the primary source.

这个工程由 `TinyPirate's DF tutorials
<http://afteractionreporter.com/dwarf-fortress-tutorials/>`_ 所启发，也是教我如何玩矮人要塞的一个教程。而这个流程开始于这些教程的升级。其他内容，要么是原始内容，要么（对于一些教程或大师课程）则是链接到最基本的源。

Consider `supporting my work <https://www.patreon.com/PeridexisErrant>`_
on Patreon.

可以考虑给该入门流程 `在Patreon上支持一下 <https://www.patreon.com/PeridexisErrant>`_ 。