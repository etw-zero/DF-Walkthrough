.. _chapter04:

######################
It almost makes sense!
######################

Welcome to Part 4 of the Dwarf Fortress Walkthrough. No nancying
around, lets get right back into it! I hope you’ve diligently completed
your homework. If you look below, you’ll see I have…

欢迎来到矮人要塞流程的第四部分。别磨叽了，让我们赶快进入这一部分吧！我还是希望你已经努力地完成了上次布置的作业了。如果你向下看的话，你将会看到我所拥有的……


Extreme Home Makeover: Dwarf Edition!
=====================================

.. image:: images/04-crafts.png
   :align: center

…Lots of Crafts! I’ve gone through and completed all of the tasks at
the end of chapter 3, and to prove it, here is a picture of my pile of
crafts. Looks like I haven’t made much in the way of bins yet, so the
crafts haven’t been tidily stored in a single bin. Don’t worry, once
bins are produced the dwarfs will sort all this out.

……好多工艺品！我已经阅读并完成了在第3章最后的所有任务，为了证明这一点，这张图片展示了我的手工艺品存储区域。似乎我现在还没有制作很多箱子，所以这些手工艺品还没有被整齐地储存在一个箱子中。但是不用担心，一旦箱子被制作完成，矮人们将会把这些产品摆放整齐的。

I’ve also got piles everywhere, workshops set up and food and booze
production going. Things are looking good! But we have yet more useful
jobs to do.

我也已经将各类库存摆放在各个地方，将工坊建造出来后，也让食物与酒品的生产步入正轨。看起来一切进展顺利！但是我们现在还有一些更多更实用的工作来完成。

Before getting to those though, let's set up a trade depot outside so
merchants can bring us wagons of useful goods.  Place that with
:kbd:`b`, :kbd:`D` outside - wagons need a three tile wide pathway
without traps, so this is the easiest way to keep it accessible.

在做这些事情之前，让我们在要塞的外面设置一个交易仓库，这样的话商人们可以带给我们一马车有用的商品。在要塞的外面，按下 :kbd:`b`, :kbd:`D` 键就可以放置一个交易仓库了——马车需要一个宽3格的入口，并且途中不能有陷阱，因此这是让其保持可通过状态的最简单的方式。

Next time traders turn up, just remember to check out the tutorial
on :ref:`tutorial-trading` for a guide to the process.

下次当交易车队出现，记得参考一下 :ref:`tutorial-trading` 的教程，来获得与此有关的指引与向导。

First up, we need to move our booze production downstairs.
I don’t want to dig out any more of our top-floor space just now,
and rock walls can be smoothed and engraved later - which keeps
dwarves happy.  Of course, we have many options for how we
expand the fortress, but we’ll go for simple right now, so lets get
shifting stuff.

首先，我们需要将我们的酒品生产挪到楼下。我现在还不想在第一层的空间挖出更多的空间，而且之后岩石墙壁也是可以被平滑且雕刻的——这能让矮人们感到快乐。当然，我们也有很多的方案来拓展我们的要塞，但是我们现在会通过简单的方式来进行，所以让我们先来移动刚才提到的一些东西吧。

Lets get get on with some digging
and our new booze space. Hell, lets move the food downstairs as well
and keep it nice and close to the dining room, where dwarfs are going
to want to eat it.

让我们继续挖掘一片地方来放置我们的新酿酒厂。嗯，然后再将食物也挪到楼下并妥善放置，放到餐厅的旁边

Here’s how I’ve plotted things out:

下图就是我放置物品的方式：

.. image:: images/04-booze-food.png
   :align: center

Once that digging is done, set up the big food storage hall as a food
stockpile. Then build a still downstairs using :kbd:`b`, :kbd:`w`,
:kbd:`l`. This should be easy-peasy for you now! Your next job is go to
upstairs and remove the old food stockpile and still.

一旦挖掘完成，就将这个巨大的食物贮藏库设置为食物库存区。之后使用 :kbd:`b`, :kbd:`w`,
:kbd:`l` 键在楼下建造一个蒸馏坊。对你来说现在这个可不算个难题！你的下一步工作就是向上一层然后将上层的食物库存区和蒸馏坊移除掉。

Look what the cat dragged in!
=============================
At some point you’re going to get immigrants. I got some right now.

在某个时刻，你可能会获得一批移民。而我现在就来了一批。

    :guilabel:`Some migrants have arrived.`
   （一些移民已经到达。）

Soon after the message a stream of new loafers streak into the
fortress. And what’s the first thing they do? Eat and drink!
This has me a little concerned about our food stocks, and if
this has happened to you, I suggest you do what I do and sort out some
more booze and dig some more bedrooms downstairs and assign those new
dwarfs to it.

消息提示后不久，一群二流子就跑到了你的要塞里了。他们首先要做的事情是什么呢？是吃吃喝喝！而这就让我比较担心我的食物库存了，同时如果这也发生在你身上的话，我建议你和我做一样的事情，整理出一些额外的酒，然后在楼下多挖一些卧室，别忘了将卧室指定给新来的矮人们。


要塞外的农业发展，乐趣与利益并存
==================================
You know about farming inside, now how about farming outside? As you
may recall we’ve had some plant gathering going on outside. In
temperate climates that means we’re generally gathering berries.
And once we’ve eaten berries, what do we have? Well, if you’re a
dwarf, you end up with seeds. And wouldn’t it be great to
plant them? Yes it would!

你应该知道怎么在要塞的内部进行作物的种植，那么在要塞外面的种植会是什么样的呢？你大概会回忆起来一开始我们在要塞的外面收集植物的时候。在目前的温度气候下，我们往往可以收集一些浆果。之后万一我们将讲过吃光了，那么我们还有什么呢？嗯，如果你是个矮人的话，你应该会留下一些种子。而种植这些种子种下去怎么样？当然可以！

We’ll cover looking at our stocks of goods later, by the way, so just
trust me for now.

顺便一提，我们等会儿再检查我们的货物库存——所以现在就请相信我吧。

But berries are going to require being planted outside, as they
love the sun don’t they? Of course, we don’t want to go outside where
it could be nasty and dangerous, so what do we do?. We can’t really
expect strawberries to grow in a dark cave, can we? So how about we
compromise with these fruity demons. We’ll build an outdoor farm, but
we’ll lock it off from the world with a wall and an entrance only
available to us.

但是浆果需要种植在外面，因为它们属于喜光植物。当然，我们的矮人们不想外出，因为那里令人讨厌，危机四伏，所以我们该怎样做呢？我们并不能期望草莓凭空就能在漆黑的洞穴里生根发芽。所以我们就不得不向这些充满诱惑的水果恶魔们妥协，给它们在外面制作一个农场，同时我们也将会给这个农场安装几面墙和出入口，只允许我们进出。

To achieve our goals we first need to dig some handy exit to the
outdoors. Perhaps near our existing farm. This is what I did:

为了达到我们的目的，我们需要先挖一个通往外面的出入口。这个出入口大概需要靠近我们现在的农场，而下面展示的就是我的方案：

.. image:: images/04-dig1.png
   :align: center

I’ve expanded the farm room a little and set a passage to the outside
to be dug. Once the space is dug you’ll notice that the slope icons
still exist around the exit. This could be a problem. If we built walls
around a nicely enclosed farm now enemies would still be able to get to
it from above, by walking down the slopes! So we need to remove the
slopes. To remove it, we hit :kbd:`d` for :guilabel:`Designations`
and then :kbd:`z` for :guilabel:`Remove Up Stairs/Ramps`.

我将农场房间扩充了一点，然后规划了一个通往外部通道的挖掘计划。一旦规划的地方挖掘完成，你会注意到出口出口处的上下坡放哨依然在存在。这可能会导致一些问题产生。如果我们绕着农场建造了一个封闭的围墙，那么敌人还是可以通过围墙上方进来，就是通过这些斜坡进来的！所以我们需要去移除这些斜坡。我们可以按下 :kbd:`d` ，代表 :guilabel:`Designations` （指定），之后按下 :kbd:`z` ，即 :guilabel:`Remove Up Stairs/Ramps` （移除向上的楼梯或上坡）。

I’ve selected almost all of the slopes across the front of my fortress.
I don’t want any surprises 'dropping' in anywhere along our front. Here
you can see my miners hard at work  stripping away the outside ramps so
there’s essentially a sharp drop between the level above and this
level.

我选择了我要塞前几乎所有的上坡。我可不想让我的要塞里面出现什么惊喜。这里你可以看见我的矿工在努力地工作，将外面的斜坡铲平，因此这里的上层和下层就会产生一个断层。

.. image:: images/04-dig2.png
   :align: center

While you’re at it, you could tidy up the outside edge of the fortress
with digging and ramp removal. Here’s my much tidier fortress entrance:

当你在做这件事的时候，你也应该清理要塞外面的边缘，也就是将那些斜坡通过挖掘的方式处理掉。这里展示一下我的要塞，入口处很明显干净多了：

.. image:: images/04-dig3.png
   :align: center

Later on we might incorporate some complex defenses into this area.

之后我们可能会在这片地方放置一些复杂的防御工事。

.. note::

    Recent versions of Dwarf Fortress added the ability to climb,
    so to be truly secure a wall must smoothed natural stone,
    or built two levels high with an overhand at the top.  Using
    stone blocks also helps.

    That's pretty tricky though, so for now we'll just hope the
    goblins don't bother.

    最近版本的矮人要塞添加了攀爬的能力，所以如果想确保一堵墙是完全安全的话，就需要使用打磨的来自自然的石头，或者将墙建成两格高，同时在顶部放置一个overhand。当然用石砖也是可以的。

    尽管这可以说十分具有技巧性，但至少现在而言我们也就只能期望哥布林不要打搅我们的生活了罢。

So, lets get on with this farm! We need to surround a nice large area
with walls, right? Keep our dwarfs safe from wandering critters. To
build walls we need to:

嗯，咱们可以开始继续建造我们的农场了！我们需要腾出一大片空地，然后将周围建造墙体。让我们的矮人们免受小动物野蛮的横冲直撞。为了建造墙体，我们需要这样做：

* Hit :kbd:`b`
* Hit :kbd:`C`, (that’s :kbd:`Shift`-:kbd:`c`, remember the keys are
  case-sensitive), or scroll through the list and look for
  :guilabel:`Wall/Floor/Stairs/Track` and hit :kbd:`Enter`.
* :guilabel:`Wall` is selected by default, hit :kbd:`Enter`.
* You now have a green X. Like the farm plot you can change the size
  with :kbd:`u`, :kbd:`m`, :kbd:`h` and :kbd:`k`.
* Hit :kbd:`u` until you’ve got a max-height wall.
* Place the wall right next to the entrance, hit :kbd:`Enter` (below you can
  see how I placed mine).
* Select a material using :kbd:`=` and :kbd:`Enter`
* Hit :kbd:`Enter` until the list goes away and you see the wall outline.
  You’re selecting one item for each segment of the wall.
* Hit :kbd:`Esc` until the game resumes.

* 按下 :kbd:`b` 。
* 按下 :kbd:`C` ，（要注意这里是 :kbd:`Shift`-:kbd:`c` ，按键是要区分大小写的），或者向下滚动，找到 :guilabel:`Wall/Floor/Stairs/Track` （墙/地板/楼梯/轨迹），之后按下 :kbd:`Enter` 。
* :guilabel:`Wall` 就是被选做默认了，直接按下 :kbd:`Enter` 即可。
* 你的屏幕上会显示一个绿色的X。就像建设农场一样，你可以通过 :kbd:`u` ， :kbd:`m` ， :kbd:`h` 和 :kbd:`k` 键开更改你所希望的大小。
* 按 :kbd:`u` 键，直到你得到了一个最大高度的墙。
* 在入口的右侧对面放置墙壁，然后按下 :kbd:`Enter` （在下面你可以看到我是怎么放置的）。
* 使用 :kbd:`=` 选择一个材料，之后按下 :kbd:`Enter` 。
* 按 :kbd:`Enter` ，直到列表小时，你就可以看到墙的轮廓了。你在为每一段墙选择物品（原材料）。
* 按 :kbd:`Esc` 键，直到游戏继续下去。

You will now have a wall under construction! Again, hit :kbd:`q` and move it
down your wall, you’ll see the construction status. Don’t worry, your
dwarfs will get to the wall pretty quickly.

你现在就有一个正在施工的墙体了！之后，按下 hit :kbd:`q` 键，然后将光标移动到墙上，你就会看见建设的进度了。不用担心，你的矮人等会儿就过去建造了。

While you’re waiting, clear all the trees and bushes from inside your
soon-to-be farm space using :kbd:`d`, :kbd:`t` to :guilabel:`Chop Down
Trees` and :kbd:`d`, :kbd:`p` to :guilabel:`Gather Plants`.

在你等待的时候，清理周围树木与灌木丛吧，范围就在等会儿将要建造农场的地方。用 :kbd:`d` ， :kbd:`t` 键选择 :guilabel:`Chop DownTrees` （砍下树木），之后按下 :kbd:`d` ， :kbd:`p` 选择 :guilabel:`Gather Plants` （采集植物）。

Once you’ve got this under way, build two lengths of wall across to the
right go down a length and a bit, and back to the cliff face.
Here’s how my outdoor farm plot looks so far:

一旦你完成了这项工作，在右侧建造两段墙，然后向下延伸一点，最后将墙建到悬崖面上。到目前位置，这是我户外农场的样子：

.. image:: images/04-build1.png
   :align: center

Isn't it coming along nicely? Soon we’ll be able to hide
inside and behind our walls and ignore the nasty outside world. Yay!

建的还不错吧？一会儿我们能藏匿于墙的里面与后面，与险恶的世界相绝。好耶！

Once your walls are complete you could easily build two 6×6 farm plots
inside this space. One point though, make sure all those trees are cut
down and plants harvested, otherwise you’re going to end up with a
patchy farm plot. Also, if there are any trees in the way of a wall
being built you won’t be able to place the wall. Get them cleared and
the problem will go away.

一旦你的墙完成了建造，你就可以轻易地在里面的空间里建造一个6*6的农场。还有需要指出的是，要确保这些树都被砍倒而且植物都被收集，否则你将会有一个杂物丛生的农场。同样，如果在建造墙的位置上有树占据，那么这个地方就无法建造墙。将这些树木与灌木清理干净，之后问题就迎刃而解了。

Here’s my private outdoors farm yard with the farm plots built as well:

这里是我的私人户外农场：

.. image:: images/04-build2.png
   :align: center

There you go! Beautiful! Now, just like farms inside, you need to
specify what the fields will build (:kbd:`q`). On the first, I’ve set
strawberries for every season (don’t forget to cycle through the
seasons using :kbd:`a`, :kbd:`b` :kbd:`c`, :kbd:`d`).
When I tried to select strawberries
on the second field they were red, suggesting to me we won’t have
enough seeds that this isn’t the season for planting those items
So instead, I
selected some other random plant. Not sure we’ve got seeds for those,
but we’ll find out all about that later! And later on you can come back
and fix up some better planting instructions. Oh, don’t select
:guilabel:`Seas Fert` or :guilabel:`Fertilize`.
We don’t have any fertilizer yet.

就是这样！漂亮极了！现在，就像当时布置要塞内部的农场一样，你需要指明这片农田在各个季节种植的作物（ :kbd:`q` ）。开始，我给所有的季节都安排了种植草莓的任务（在配置的时候注意切换一下季节，使用 :kbd:`a`, :kbd:`b` :kbd:`c`, :kbd:`d` 键来切换）。当我尝试在第二片田地里种植草莓的时候，田地显示了红色，提示表示我们没有足够的种子，所以这个这不是种植草莓的季节，因此我就换用了其他随意的一些植物。我们不确定现在有没有种子，但是我们等会儿就能知道所有的消息了。之后你可以回来然后修正一些种植的方案。对了，不要选择 :guilabel:`Seas Fert` （海洋肥料）或者 :guilabel:`Fertilize` （肥料）选项，因为我们目前还没有肥料。

While I remember things, lets take a moment to build a wall along the
top edge above our outside farm. We don’t want any goblins walking up
to the edge of the cliff, looking down, and shooting up our farmers
with their crossbows! So go up a level, using good-old :kbd:`<` and
plan out your wall. Here I’ve built a wall,
which should once and for all block off any possible approach to my farm.

对了，我又想起来一些事情了，让我们在农场靠近悬崖的一侧再建一堵墙。我们可不想让哥布林走在悬崖边上，朝下一瞅，然后端起十字弩朝我们的农民射击。所以将视角向上一层，使用 :kbd:`<` 键然后规划你的墙，这里就是我自己建立的墙，这应该能一劳永逸地解决进入我农场的任何途径。

.. image:: images/04-build3.png
   :align: center

There's one last thing we should take care of today - hungry animals!
It's pretty common for livestock to need grass (or cavern moss, or...)
to graze on, so we'd better set up a pasture zone.

今天还有一件事情需要我们注意一下——饥饿的动物！家畜需要吃草（或者洞穴苔藓等等），这是天经地义的事情，所以我们最好还是继续规划出一片牧场区域。

* Head up near the wagon, where there's open space and plenty of grass.
* Hit :kbd:`i` for a zone, select a large area, then :kbd:`n` for pasture.
* Hit :kbd:`N`, then scroll with :kbd:`=` and select grazing animals
  with :kbd:`Enter`.

* 来到棚车的区域，那里有开阔的空间与很多野草。
* 按下 :kbd:`i` 建立一片区域，选择一大片区域，之后按下 :kbd:`n` 指定为牧场。
* 按下 :kbd:`N` ，之后使用 :kbd:`=` 键滚动，选择放牧的动物，之后按下 :kbd:`Enter` 。

.. image:: images/04-pasture.png
   :align: center

Done!  Our livestock will now happily graze under the trees, turning
grass into a foundation of dwarven industry and cuisine.

完成了！我们的家畜现在就会在树下开心地吃草了，将草转换为矮人们工业与美食的基础。

I'll see you in `chapter five <chapter05>` to look at expanding our
industries to something truly impressive!

我已经迫不及待地想让你进入 `第五章 <chapter05>` ，看着我们的工业体系是如何扩展到让人印象深刻的阶段吧！