.. _chapter01:

###############
入门
###############


Dwarf Fortress is an extremely interesting and complex game, but one
which people find exceptionally difficult to get into. Although it
isn’t hard to find tutorials, many assume some basic knowledge of the
interface and what the player is looking at. They also don’t take into
account different tilesets and versions and the difficulty new users
have in finding a good site for that first fortress.

矮人要塞是一个十分有趣且复杂的游戏，但是人们往往会发现这个游戏想要深入玩下去的话，是十分困难的。尽管找教程并不困难，而且这些教程会介绍一些与界面有关的基础知识，以及介绍玩家所看的事物。与此同时，他们也没有考虑不同图像包和游戏版本，以及忽略了新玩家如何为自己的第一个要塞寻找合适的地点这个难题。

But all that is in the past Dwarf Fortress Walkthrough! It comes with a
pre-made world and save and full game download for you to play along
with. And a ton of screenshots. Yes, many, many screenshots.

但是所有的这些都包含在以往矮人要塞游戏教程里！他附带一个预制的世界和存档，以及完整游戏的下载，你可以游玩这些存档。除此之外，还有很多的截图。对，很多很多的截图。

So grab a drink, get your thinking cap on and be prepared for some
dwarfy fun! And carp. Never forget the carp…

所以拿起自己的饮料，让你的思维活跃起来，然后准备体验一些矮人们的乐趣！对了，不要忘了鲤鱼，永远不要忘记鲤鱼……

配置
======
:DFFD:`Download the walkthrough pack here. <11171>`
:DFFD:`Download the walkthrough pack here. <11171>`

This tutorial uses a pre-configured install of DF, with the same
save and all the extras you need - but no more.  If that's not
going to work for you, see the guide to :ref:`setting-up-df`.

这个教程使用了矮人要塞的预置设置，使用相同的存档和所有其他你需要的额外事物——没有其他的繁文缛节。如果这没有正常工作的话，那么就去查阅这个教程： :ref:`setting-up-df` 。

After downloading the pack, you need to unzip the folder onto your hard
drive (but *not* in ``Program Files``). Then create a shortcut on your
desktop leading to the launcher or directly to ``Dwarf Fortress.exe``.

在下载完成这个整合包后，你需要解压这个压缩包到你的硬盘中（但并 *不是* 指你的 ``Program Files`` 文件夹）。之后在你的桌面上创建一个快捷方式，指向文件夹里的 ``Dwarf Fortress.exe`` 。

启动游戏
============
Now we’re ready to get going! Click on your new shortcut, hit "Play" if
you're using the launcher, and you should get the screen below.

现在我们准备出发了！点击你刚才新建的快捷方式，如果你使用启动器的话，按下“Play”（开始游戏），就可以显示下面的界面了。

.. image:: images/01-start-screen.png
   :align: center

So, you’ve got the game running and enjoyed the exciting ASCII intro
movie. Well done! I can tell you’ll go far! Next step is to hit
:kbd:`Enter` on :guilabel:`Continue Playing`, which should be
highlighted. You’ll then be presented with this screen.

所以，你就可以让游戏启动，之后享受激动人心的ASCII开场动画了。干得漂亮！我可以告诉你你将能走得更远！下一步是在主界面的 :guilabel:`Continue Playing` （继续游戏）按下 :kbd:`Enter` ，应该是高亮显示的选项。你将会看到下面的界面。

.. image:: images/01-save-list.png
   :align: center

This is the save game list, which gets cluttered very quickly as
seasonal saves build up. But for now all you have to do is use the
down-arrow key (not the number pad arrow key) to select
:guilabel:`region1-post-embark`. Hit :kbd:`Enter` and the game will load.

这是游戏的存档列表，会随着季节性的存档操作而变得越来越杂乱无章。但是现在你所需要做的事情是使用方向下键（不是小键盘上的下箭头）选择 :guilabel:`region1-post-embark` ，按下 :kbd:`Enter` ，之后游戏就会开始读取了。

第一印象
==========
Once the game loads you will be presented with this screen. Although
it’s confusing on first glance, don’t panic! It will all be made clear
shortly. First thing though, hit :kbd:`Space` to pause the game.
:kbd:`Esc` backs out of any menu you’re currently in.

一旦游戏加载完成，你将会看到下面的界面。尽管简单一瞥，看上去这十分令人困惑，但请不要慌乱！你很快就能搞明白这些玩意了。第一件事情，先按下 :kbd:`Space` 暂停游戏。按下 :kbd:`Esc` 回到你所在的菜单界面。

.. image:: images/01-local-area.png
   :align: center

Lets talk about what you can see. First up, the screen is divided into
three sections: The left shows the local view. The center shows the
action menu, the right shows the area map. To make things a lot neater
hit :kbd:`Tab`, which cycles through various menu arrangements. Stop
when your view matches the one below.

现在就让我们讨论一下你所看到的界面吧。第一，整个屏幕被分为了三个部分，左侧显示了当前本地的视图，中间显示了动作菜单，右侧显示了当前区域的地图。如果希望让整个界面更加简介，按下 :kbd:`Tab` 键，即可在几种不同的布局界面进行切换。当你的视图和下面的视图相同时，请停止切换。

.. image:: images/01-narrow-menu.png
   :align: center

Now we’re really cooking with gas! Soon you’ll be melting foes with
magma-falls, and drowning goblins in artfully engineered traps! But
before then, lets look around. Use the arrow keys to navigate around
the local area. You’ll notice the we’ve got a decent amount of space to
work with.

现在我们干得很漂亮！不久你就会将侵略者融化在溶岩瀑布中，让哥布林淹没在充满艺术感与工程化的陷阱中！但是在此之前让我们在四周看看。使用方向键来浏览周围的本地区域。你将会发现我们已经有了充足的空间来进行这些设计。

While you examine your surroundings you should be able to spot tree
trunks and the grassy areas easy enough, same with (frozen) ponds and
the stream and bushes. Other tiles won’t make much sense, these tend to
be the slightly-arcane zones like open air spaces (pale blue tiles, or
tiles with dots) and the earth (brown tiles) and slope or ramp tiles (upward
triangles). The screenshot below should help point those features out.

当你在检查你周围的环境时，你应该能轻易地看到树桩与草绿色的装饰，还有（上冻的）池塘、溪流和灌木。其他的地块则没有什么太大的意义，这些地块都更加倾向于略微晦涩的区域，比如开放的空气空间（淡蓝色地块，或者点状地块），以及地面（棕色地块）和斜坡与坡道地块（向上的三角）。下面这张截图能够帮助你展示刚才所提及的特点。

.. image:: images/01-features-lvl108.png
   :align: center

Key to understanding DF’s local view is getting your head around the
fact that DF is a 3D game displayed on a 2D map. To display a variety
of heights the world is sliced into dozens of Z-levels. Each Z-level is
numbered.

理解矮人要塞地区视图的关键，在于你的头脑应该理解一个事实，那就是矮人要塞是一个3D游戏投影在一个2D地图上。为了显示世界上不同的高度，整个世界被切片为了数量较多的Z轴。每一层Z轴都被编号。

If you look on the bottom right of the window you’ll see we’re
currently at level 108. To move between levels you need to hit :kbd:`<`
to go up, and :kbd:`>` to go down. If you go up a level the map will
look like image below. Try it yourself now.

如过你看到了地图的右侧底部，你会看到我们所处的高度为地108层。如果要移动当前视角的地图层数，你需要按下 :kbd:`<` 来到上一层，按下 :kbd:`>` 来到下一层。如果你的视图是向上移动的，那么地图显示就像下面所展示的图片一样。现在你可以去尝试一下。

.. image:: images/01-features-lvl109.png
   :align: center

You’ll see that what was black dirt is now grass and trees. It’s like
we’ve gone up a floor in a lift and we’re no longer looking at a slope,
now we’re looking across a grassy plain. Below us is open space and
tree tops. Make sense? Now, try going back down to 108 and then down to
107, a level below "ground" level.

你将会发现刚才显示的黑色泥土变成了草地与树木。这就像我们坐在电梯里向上走了一层，同时我们不再看到斜坡，现在我们所看到的则是一片草绿色的平原。在我们的下面则是空旷的区域与树木的顶部。这合理吗？现在，尝试回到第108层，之后再向下来到107层，这层是“地面”的下一层。

.. image:: images/01-features-lvl107.png
   :align: center

Your view should look something like this. We’re underground now with
mostly dirt around and earth between the pool, the tree roots, and the
rest of the ground. You may have noticed the pool doesn’t appear to
have changed much, but don’t be confused. The difference is that on
this lower level we’re on the same level as the body of the pool
itself, at the level above, we’re actually above the pool a little
looking at the top of it.

你的视图会像这张图所显示的一样。我们所在的地下层所展示的吃绝大多数的泥土，和地面上零星的池塘、树根，以及其他的地面区域。你可能也已经注意到到了池塘好像也没有改变太多，但是不要被迷惑了。这两者的区别在于在这偏低的一层，我们所处的平面和池塘主体所处的平面相同，而在这一层之上，我们则实际上是在看池塘顶上的上面一层。

Continue having a look around. When you’re done, return to Z-level 108
and find the wagon with our dwarves. The helpful graphic below explains
what you’re looking at.

继续去观察周围。如果你做完了这些动作，那么就回到108层，然后找到我们围绕在货舱周围的矮人们。下面的这张帮助视图能够为你解释你所观察的事物。

.. image:: images/01-unit-detail.png
   :align: center

让我们开工！
=================
So now what do we do? In Dwarf Fortress we’re usually trying to get a
bustling community of dozens of dwarves up and running, while fending
off goblins, the whims of nobles and assorted other evils. To do this
we need to build ourselves a mighty fortress! Preferably underground.
So lets find a cliff face to dig into and get this show on the road!

所以说现在我们需要干嘛？在矮人要塞里，我们经常会调动包含十几甚至几十名矮人的社区，让这个社区正常运作下去，同时避开哥布林、奇幻贵族与各类邪恶。为了做到这一点，我们需要建立一个强有力的要塞！尤其推荐的是将要塞建设在地底之下。所以让我们去找到山崖的一侧，向里挖掘并且让其显示在路上！

If you go west (left) from where our dwarves are (using the arrow keys)
you’ll see a nice cliff face. Lets dig an entrance into this spot and
plan to eventually dig down way underground where it’s safe and cosy.
First up, get the map centered around about where we want to dig (shown
on the screenshot below). Once you’re ready:

如果你向矮人所在的地方向西（左侧）查看（使用方向键），你会看到一个不错的山崖断面。让我们在这个地方挖掘一个入口，之后规划最终向下挖掘的道路，通往我们安全又舒适的地下。首先，让我们的地图显示在我们想要去挖掘的区域（就像下面所展示的截图一样）。当你准备停当后：

* Hit the :kbd:`d` key. You’ll see the menu on the right changes and in
  the local view a yellow :guilabel:`X` has appeared. The menu on the
  right should show the :guilabel:`Mine` option highlighted. If it
  doesn’t, hit :kbd:`d` again, to select it.
* Move the cursor to the edge of the cliff, like this:

* 按下 :kbd:`d` 键。你将会看到左侧的菜单发生了变换，在本地视图中也显示了一个黄色的 :guilabel:`X` 。地图的右侧 :guilabel:`Mine` （采矿）选项被高亮。如果这个选项没有被高亮，那么就再次按下 :kbd:`d` 键来选中它。
* 将光标移动到山崖的边缘，就像下面所展示的一样：

.. image:: images/01-dig-cursor.png
   :align: center

* Now hit enter and move your cursor down with the arrow keys. You’ll
  see that you’ve dropped a flashing "anchor". This is going to mark out
  what your dwarves will dig. Move it across 20 and up 3 and hit
  :kbd:`enter`, your screen should look like this:

* 现在按下回车键，用方向键控制你所看见的光标。你将会看到你留下了一个闪烁的“锚”。这个提示标记了你的矮人将会挖掘的地方。将光标移动20格，向上移动3格，再次按下 :kbd:`enter` （回车键），你的屏幕看起来会像下图一样：

.. image:: images/01-dig-hallway.png
   :align: center

The browned out area shows where your miners are going to come along
and dig. But they won’t act until you back out of the
:guilabel:`Designations` menu as the game is paused. Hit :kbd:`Esc` and
you should see the game unpause and the menu reset to its master list.
Oh, when you have the :kbd:`d` menu up you can actually click on the
map with your mouse and select areas to mine. Some people prefer to
select areas this way.

棕色展示的区域显示了你的矿工将会到达并挖掘的地方。但是他们可能不会行动，除非你暂停游戏并回到 :guilabel:`Designations` （设计）菜单。按下 :kbd:`Esc` 键，你可能会发现游戏开始运行，菜单也回到了刚才的主列表中。嗯，当你按下 :kbd:`d` 键打开菜单，你可以用鼠标（或键盘）选择你所指定的挖掘区域。有些人更加偏爱于使用这种方式来指定任务。

With the game unpaused you should notice two dwarves race to the cliff
face and start digging. With our entrance under way we should also
think about laying out some rooms for our dwarves to live in, who wants
to spend time out under that hot yellow disk when lovely rock and earth
beckon!

当游戏运行时，你应该可以注意到有两个矮人跑去了山崖面上，并且开始挖掘。当我们的入口正在挖掘时，我们需要思考一下如何布局规划一些房间，以为我们的矮人提供住宿的环境。毕竟谁想花时间在烈日之下躺在可爱的石头与地面上呢！

Right now we’re not worried about making our fortress perfect and
creating the strongest entrance, we’re simply trying to scratch out a
space to live! To that end we’ll need three or four rooms off this main
entrance.

现在我们并不会关心该如何让我们的要塞变得完美，让我们创造一个最坚固的入口。我们现在仅仅想腾出来一块地方来生活！为了这个目的，我们需要在这个入口的里面扩充三到四个房间。

See if you can match the layout below by marking out areas to dig, as
you’ve learnt. If you make a mistake you can hit :kbd:`x` from the
:guilabel:`Designations` (:kbd:`d`) menu and you’ll note that the menu
on the right has :guilabel:`Remove Designation` highlighted. Now when
you hit enter and select an area, any area set for digging will be
cleared of that designation. Anyway, enough detail, on with the room
building!

来测试一下，你能不能挖掘如下图所示的区域，就像你刚才所学到的那些内容。如果你不小心规划错了，你可以在 :guilabel:`Designations` （设计）菜单（ :kbd:`d` ）按下 :kbd:`x` 键，你会发现右侧的菜单中 :guilabel:`Remove Designation` （移除设计）被高亮了。现在当你按下回车键，并且选择一个区域，所有在该区域准备规划的挖掘任务将会被取消，正如你所规划的这般。无论如何，现在是给足了所有建造房屋的细节了！

.. image:: images/01-dig-rooms.png
   :align: center

Look at the little dwarves go! Aren’t they industrious! They’re quickly
digging away and leaving a lot of dirt floor behind them. Good lads!
Lets leave them to their work while we sort out some other important
jobs.

看看这些小小的矮人们忙碌的身影！他们是不是很勤勉！他们快速地挖掘，并且在他们的身后留下了很多的泥土地面。好伙计！就让我们看着他们忙碌工作的身影，顺便再看一下其他重要的工作。

砍些树，摞起来！
================================
Dirt and rocks are handy, but so is wood to make nice dwarven beds, and
we’re short on it right now. To get wood, we need to chop down some
trees. And we do that by designating an area of trees to be chopped.
Lets get cracking and clear the trees in front of the entrance.

泥土和石块是随处可见，但是正因为如此木头才能成为矮人们床的材料，以及现在我们正缺乏木材。为了获取木材，我们需要去砍到一些树木。在这时，我们可以通过指定一片树林区域来说明这片树木需要被砍到。让我们砍掉门口前的树吧。

Designating trees to chop is like designating rocks to be dug, so lets
decimate the local environment!

指定砍伐树木的区域，其实和指定需要挖掘的石头是一样的。所以咱们现在就开始改造这里的自然环境吧！

* Move the map around so you’re looking out front of your fortress.
* Hit :kbd:`d`. Hopefully this time you’ll notice the game has
  automatically got :guilabel:`Chop Down Trees` selected, if it doesn’t,
  hit :kbd:`t` and it will be highlighted.
* Move the cursor to the upper left, hit :guilabel:`Enter`, and move
  the cursor to the bottom right, hit :guilabel:`Enter` again. Any trees
  in that area will now be set to be cut, as indicated by them all being
  marked brown.

* 移动地图的周围，这时你可以看到要塞之外的区域。
* 按下 :kbd:`d` ，然后希望这次你能够注意到游戏已经自动选择了 :guilabel:`Chop Down Trees` （将树砍倒）选项。如果这个选项没有被选中，那么就按下 :kbd:`t` ，让这个选项高亮起来。
* 将光标移动到左上角，按下 :guilabel:`Enter` 键，之后将光标移动到右下角，再次按下 :guilabel:`Enter` 键。所有在这片区域里的树木都会被砍伐，同时通过标记为棕色来指示被砍的树木。

Although you’ve done well neither man nor dwarf can live on wood and
dirt alone! Luckily for us there are plenty of bushes out there loaded
with ripe berries. Lets set some of them to be picked:

Although you’ve done well neither man nor dwarf can live on wood and
dirt alone! 那么多的灌木上挂着沉甸甸的熟浆果，这对我们来说是挺幸运的。所以我们来采集一些浆果吧：

* Hit :kbd:`d` again and then :kbd:`p`. One the right :guilabel:`Gather
  Plants` is now selected.
* Designate about the same area as we did with the wood cutting.
* All the bushes will be highlighted now, and when you unpause you
  should see a plant harvesting dwarf join the woodcutter.

* 在此按下 :kbd:`d` ，之后按下 :kbd:`p` 。现在右侧的 :guilabel:`Gather
  Plants` （收集植物）选项被选中了。
* 就像我们刚才设置砍树区域那样，设置相同的区域。
* 所有的灌木丛都被高亮选中了，之后当你继续游戏的时候，你会看到一个负责收获植物的矮人屁颠颠地跟着伐木工一块去了。

Your view now should look something like this, note the brown
'highlights' and fallen logs:

你现在的视图应该看起来和这个类似，要注意棕色的“高亮区域”和遗落的原木：

.. image:: images/01-plants.png
   :align: center

Now lets sit back and watch the dwarves work for a minute! It won’t
take long before our miners have cleared out our temporary living
quarters and our woodcutter and plant gatherer have begun their work.
Once the interior space is clear we have lots of work to do, so lets
break it down into useful chunks.

现在我们坐与放宽，在这一分钟内看着矮人们工作的场景吧！我们的矿工清理完临时住所区域、伐木工和植物收集者们开始他们的工作，其实这些都不需要太多的时间。一旦矿工清理完区域，我们就会有许多需要做的事情，所以我们来分配这些区域并且将之好好利用一下。

第一个农场
==============
Farming is how you will make most of your food in Dwarf Fortress, and
it’s important to get farming quite quickly. If food runs out your
dwarves will starve and your game will end prematurely, and we don’t
want that, right? dwarves usually farm underground and handily, we have
a space all prepared. So follow along, and lets get some tasty
mushrooms growing for our stumpys to chow down on!

在矮人要塞里，作物种植是你获取绝大多数食物的方式，因此建造一个农场绝对是当务之急。如果你的食物储备耗尽，你的矮人们将会饿死并且你的游戏将会早早地结束——而这并不是我们想要看到的，是不是？矮人们通常都在地下手工种植作物，而我们用于种植的空间已经准备好了。所以按照以下的步骤，让我们培育一批美味的蘑菇让我们的矮子们大快朵颐吧！

* Find the bottom right room.
* Hit :kbd:`b` for the :guilabel:`Building` menu and then :kbd:`p` for
  :guilabel:`Farm Plot`.
* Move the cursor to your farm area.
* The screen instructions tell you how you can increase the size, we
  need a good 6 by 6 plot, so hit :kbd:`u` and :kbd:`k` a few times until
  you’ve got a big green grid, like the one below.
* Move the grid around with the arrow keys until it’s in about the same
  position as the one in the screenshot. If you get the size wrong,
  :kbd:`h` and :kbd:`m` will reduce the plot.

* 找到右下角的房间。
* 按下 :kbd:`b` 键，即可打开 :guilabel:`Building` （建造）菜单，之后按下 :kbd:`p` 来建立 :guilabel:`Farm Plot` （种植土地）。
* 移动光标到你的农场区域。
* 屏幕上的指示将会告诉你如何扩大你选择的区域，我们需要划出来6x6的区域，所以按几下 :kbd:`u` 和 :kbd:`k` 直到出现了一片绿色的栅格，就像下图所示。
* 使用方向键来移动这个栅格，让这片栅格和下图所示的区域相同。如果你的尺寸标错了，按下 :kbd:`h` 和 :kbd:`m` 键来减小区域大小。

.. image:: images/01-farm-plot.png
   :align: center

* Hit :kbd:`Enter` and the green area marker will change to a flashing
  brown field marker. When stuff flashes it indicates that dwarves are on
  their way to come build the construction, in this case, a field. And
  lookee-here! A farmer has come to build our field for us!

* 按下 :kbd:`Enter` ，这片绿色区域的标记将会变化为闪烁的棕色区域标记。当材料闪烁时，这表明矮人们已经前往建立结构的路上，在这次的操作里，结构是一片区域。之后看看这里！一个农民已经为我们建立了一片农田。

.. image:: images/01-farm-build.png
   :align: center

He will take a short while to build your field. When you see the dwarf
run off, you know he’s done. Now it’s time to set the field to grow
delicious plump helmets all year round.

他将会花费很短的时间来建立这片区域。当你看到矮人跑掉了，你应该直到他的工作完成了。现在是时候来让这片农田在这整年中生产美味的肉盔菇了。

* Hit the :kbd:`q` key. This is the key for :guilabel:`Set Building
  Tasks and Preferences`. Again you’ll notice a cursor on the local
  screen and if it’s anywhere near the field, the field will be flashing.
  You can move that cursor around with your arrow keys, this is useful
  later on when you need to select different workshops and buildings.
* You’ll note that on the right the menu now shows you specific task
  details associated with that field, like so:

* 按下 :kbd:`q` 键。这个按键对应菜单里的 :guilabel:`Set Building
  Tasks and Preferences` （设置建造任务和优先级）菜单。再次按下这个键，你会注意到一个光标显示在屏幕的中间。如果它的位置在农田的附近，农田将会闪烁标识。你可以用方向键移动那个光标，当你待会儿需要选择不同的工坊和建筑，这将十分有帮助。
* 你会注意到在右侧的菜单显示出这片农田相关的任务细节，就像这样：

.. image:: images/01-crop-select.png
   :align: center

* What we want to do is set :guilabel:`Plump helmets` as the food to be
  planted every season. You can see on the top right of the menu we have
  the various plants we could try and grow and in the middle area we have
  the different seasons. Right now :guilabel:`Dimple cups` are selected
  (but not highlighted, note) and the season is set on
  :guilabel:`Spring`, as it’s Spring currently. We need to change these
  settings.
* Now comes an important skill! Menu scrolling! Using :kbd:`=` (*not*
  the down-arrow), scroll down through the list to :guilabel:`Plump
  helmets` and hit enter.  You should now see :guilabel:`Plump helmets`
  highlighted. By the way, :kbd:`-`/:kbd:`=` is the way to scroll in
  menus; DF usually uses - and +, but having to use the shift key gets
  annoying.
* Good work! But right now we’ve only set the spring planting. It’s
  time to set the planting for Summer, Winter and Autumn too. To do this,
  with the field task menu up, hit :kbd:`b` for Summer and then scroll to
  :guilabel:`Plump helmets` and hit enter again. Hit :kbd:`c` for Autumn,
  set :guilabel:`Plump helmets` and then :kbd:`d` for Winter, repeating
  your selection. You may have noticed that what you can plant changes
  with the seasons, but don’t worry about that for now.
* Hit :kbd:`Esc` to back out of the menu and resume the game. Your
  dwarves will quickly begin planting in your new field, well done!

* 我们需要做的是设置 :guilabel:`Plump helmets` （肉盔菇）作为四季需要种植的食物。你可以看到在菜单的右上角，我们有很多类可以尝试并种植的作物，可以在中间的区域我们有不同的季节。现在 :guilabel:`Dimple cups` （Dimple cups）被选中（但是要注意这里并没有被高亮），同时现在的季节被设置为了春季，而且现在正是楚天。我们需要改变这些设定。
* 现在需要学习一个十分重要的技能！滚动菜单！使用 :kbd:`=` （ *不是* 下箭头），向下滚动菜单，直到移动到 :guilabel:`Plump helmets` （肉盔菇）并按下回车键。你应该能看到 :guilabel:`Plump helmets` （肉盔菇）选项被高亮。顺带一提， :kbd:`-`/:kbd:`=` 是菜单滚动的方式；矮人要塞一般使用 - 和 + 键，但是使用Shift键来按 + 键十分恼人。
* 好样的！但是现在我们仅仅设置了春季的作物。现在是时候来设置夏季、秋季、冬季的作物种植了。为了达成这点，让区域任务打开，按下 :kbd:`b` 来切换到夏天，之后滚动菜单到 :guilabel:`Plump helmets` （肉盔菇）并再次按下回车。按下 :kbd:`c` 键换到秋季设置为 :guilabel:`Plump helmets` （肉盔菇）后按下 :kbd:`d` 键换到冬季，重复同样的动作。你可能已经注意到了你可以根据不同的季节来种植不同的植物，但是至少现在我们还不用担心这点。
* 按下 :kbd:`Esc` 键回到菜单，让游戏继续运行。你的矮人将会快速地在这片农田上种植作物。干的漂亮！

Now we’ve got a field down hopefully none of your dwarves will starve,
go crazy and resort to eating rats, or each other. Pretty soon you’ll
have a few seeds in the ground and your farm will begin to look a bit
like this:

现在我们已经建立了一片农田，并且希望我们的矮人们不会挨饿，变得发狂并且通过吃掉老鼠或者同类果腹。在不久之后你会看到一些种子洒在这片土地上，你的农场将会向下图一样：

.. image:: images/01-crop-planting.png
   :align: center

Oh, by now you may have noticed the announcements along the bottom of
the screen. This is generally because the game wants to let you know
that something important has happened. You can hit space to let the
game resume, or hit :kbd:`a` to see the alert if you miss it at the
bottom of the screen). Hitting :kbd:`Esc` will take you back out of the
alerts menu and resume the game.

呀呀，现在你可能已经注意到了屏幕底下的通知了。这是十分稀松平常的，因为游戏想让你注意到有些重要的事情已经发生了。你可以按下空格来让游戏继续下去，或者如果你不小心错过了这条消息，按下 :kbd:`a` 键来查看这条提示。按下 :kbd:`Esc` 将会让你退出通知列表并且让游戏继续运行下去。

库存
==========
While you can just leave all of your dwarves’ stuff strewn around the
countryside, it’s much more efficient to have it all inside near where
it’s needed. You may have noticed your farmer dwarves running in and
out of the fortress to grab the seeds that they need (when they flash
between their icon and a little red dot (the seed icon) you know they
are carrying seeds).

你当然可以让矮人们的物资散落在城镇的每一个角落，但是如果能让这些物品收集在所需要的区域中，这会更加富有效率。你应该能注意到你的矮人农民在穿梭在要塞内外，拿取他所需要的种子（当他们在本人的图标和一个小的红色点（种子图标）之间闪烁，你会直到他们正在搬运种子）。

This is where stockpiles come in. They make everything more organised.
A stockpile for all our food and seeds right next door to the farm
would probably be pretty handy, right? We don’t want the dwarves
running across the map to get a bite to eat or a seed to plant, so lets
make a food stockpile inside!

这就是需要设定库存区域的必要了。库存可以让一切变得更加有组织。一个储藏所有食物与种子的区域，安置在农场的隔壁，这可是真的有效率，不是吗？我们不想让矮人们为了一口食物穿越大半个地图，也不想让他们为了搬运一个种子或植物而跑这么远的距离，所以让我们在要塞内部制作一个食物库存吧！

* Find the room opposite the farm.
* Hit the :kbd:`p` key (for "piles" of course), a cursor will appear.
* Hit :kbd:`f` to select :guilabel:`Food`, we want to make a food
  stockpile, after all.
* Move the cursor to the bottom right of the room, hit enter, move it
  to the top center and hit enter again. You have now set that space as a
  food stockpile, well done!
* Hit :kbd:`w` to make a :guilabel:`Wood` stockpile, and place it on
  the other side of the room.
* After a few seconds your spare dwarves will start moving food inside,
  you’ll see barrels and bags being hauled and pretty soon the stockpile
  will look something like this:

* 找到农场隔壁的房间。
* 按下 :kbd:`p` 键（当然是“区域”功能了），一个光标将会出现。
* 按下 :kbd:`f` 键来选择 :guilabel:`Food` （食物），毕竟我们需要指定一片食物库存。
* 将光标移动到房间的右下角，按下回车；将光标移动到房间的中间靠上的位置，再次按下回车。你就会将这片区域设置为食物库存了，真棒！
* 按下 :kbd:`w` 键来指定一个 :guilabel:`Wood` （木材）库存，之后将它放置在房间的另一侧。
* 在几秒之后，你的空闲的矮人们将会开始向里面移动食物，你将会看到木桶和包裹被拖进去，一会儿库存就会像下图一样：

.. image:: images/01-first-stockpiles.png
   :align: center

You don’t have much room inside the fortress yet, so no more piles for
now. In time you’re going to want to put stockpiles everywhere to help
manage your production and resource gathering. For now, well done,
you’ve got food production up and running, you’ve set up a stockpile,
you’ve set wood to be chopped and plants to be gathered and you know
how to do some basic digging! You’ve come far!

你现在的要塞里面没有多少房间，所以目前没有更多的库存区域了。你肯定想让库存尽快遍布合适的位置，让你的生产和资源收集更加便于管理。现在，不错，至少现在的食物生产已经建立并开始运作，你设置了一个库存，你砍伐了木头并且收集了植物，同时你还直到了怎么做一些简单的挖掘工作！你已经走在这条路上了！

Before reading Chapter 2, how about setting some more plants to be gathered
and more wood to be chopped. Then, head `to the next installment <chapter02>`
and we’ll learn all about workshops, bedrooms, dinning rooms and stairs! It
will be ever so exciting, I promise!

在阅读第二章之前，安排一下砍伐更多的树木收集更多的植物如何？之后前往 `进行下一步建设 <chapter02>` ，然后我们将会学习所有关于工坊、卧室、餐厅和楼梯！这一章将会十分激动人心！我保证！
