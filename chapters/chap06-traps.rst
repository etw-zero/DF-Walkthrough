.. _chapter06:

############
这是个陷阱！
############


Traps form the majority of many people’s defences, so it’s best we get
sorted and make some. First up, we will need a lot of mechanisms, so go
find that mechanic’s workshop and fill it’s job queue with mechanisms.
They’ll end up in a finished goods pile when done.

陷阱是大多数人的主要防御措施，所以我们最好还是准备开始做一些吧。首先，我们需要很多机械装置，所以我们来找到机械工坊，然后将任务队列塞满机械装置的任务。任务做完的时候，它们会出现在成品的储物区。

Next, go to the Carpenter’s workshop and add a ton of cages (:kbd:`j` is the
shortcut from the :guilabel:`Add Task` menu). Try and get ten built. Add another
carpenter’s workshop to your workshop floor if you fancy, makes it
easier to queue up more than a few of any item and get things made
faster.

下一步，来到木工坊，制作很多很多的笼子（在  :guilabel:`Add Task` （添加任务）中的快捷键为 :kbd:`j` ）。先做10个吧。如果可以的话，在你的工坊层再添加一个木工坊，可以更加方便地安排任务，同时也能提高制作物品的效率。

Defence and Traps! 防御与陷阱！
==================
You’ve got mechanisms, you’ve got cages, now lets make some traps! Head
to the front entrance of your fort and we’ll get building traps and
then ponder the various strategies one might employ in setting up the
defence of your fortress. Follow along:

你有了机械装置，有了笼子，接下来让我们来做一些陷阱！来到要塞的入口处，我们将会建造一些陷阱，然后再考虑几种不同的策略，来让这些陷阱保护我们的要塞。跟随下面的步骤：

* At the front entrance, hit :kbd:`b`.
* Scroll to :guilabel:`Traps/Levers` and hit :kbd:`Enter`, or just hit :kbd:`T`.
* You will now see a list of traps, lets briefly discuss them.

* 在要塞的入口前端，按下 :kbd:`b` 。
* 找到 :guilabel:`Traps/Levers` （陷阱/拉杆），然后按下 :kbd:`Enter` ，或者直接按下 :kbd:`T` 。
* 你现在会看到陷阱的列表，让我们简短地介绍一下吧。

:Stone-fall traps:
    These are simple one-shot traps that drop a big rock on an enemy’s
    head. These are great for a quickly built defence, but clearly, as
    one shot traps, a little limited. Note, with all traps, if your
    dwarfs fall onto them, unconscious, they will trigger the trap!

:落石陷阱:
    这是一种简单的一次性陷阱，可以让一个巨大的岩石砸到敌人的头上。这是一个快速建造防御的不错选择，但是很明显，作为一个一次性陷阱，它也有局限性。注意，所有的陷阱，如果你的矮人不小心或者是无意走上去，他们也会触发这些陷阱！

:Weapon Trap:
    These will probably be your bread and butter as they will attack
    any enemy triggering them and with multiple weapons can be quite
    powerful.

:武器陷阱:
    这大概是最基本的一种陷阱了，它会利用多种强大的武器，攻击那些触发陷阱的敌人。

:Lever:
    Not a trap, but a device used to manually trigger things like
    bridges, cages, doors, floodgates, supports, etc. More on these
    later some time.

:拉杆:
    并不是陷阱，但是它是一类装置，用以触发一些装置，比如桥、笼子、门、闸门、支柱等装置。这些东西以后会谈。

:Pressure Plate:
    Can be part of a complex trap or can be used to trigger other
    objects when conditions are met (such as water being on them,
    creatures stepping on them, magma flowing over them, etc). More on
    those later too.

:压力板:
    可以是复杂陷阱的一部分，或者如果条件满足的时候（比如流水流过去、有生物走上去、或者岩浆流过去，等等），也可以用于触发其他物品。细节也是等会儿再说。

:Cage Trap:
    What we’re going to be working with in a moment! A trap that
    catches the enemy in a cage. You can then do amusing things with
    the trapped bad guys, like tossing them off a tall bridge, dropping
    them into magma, or stripping them and using them for military
    sparing practice. Good fun!

:笼子陷阱:
    这就是现在我们要做的陷阱！这是一个能将敌人困在笼子里的一个陷阱。你可以对被抓起来的坏东西做任何范特西的事情，比如让他从桥上体验一下动能，把他丢到岩浆里，或者让他们成为训练军队的飞镖靶子。真好玩！

:Upright Spear/Spike:
    A peculiar sort of trap device that can only be triggered by a
    pressure plate or lever.

:直立的矛/尖刺:
    一类特殊的陷阱装置，只能由压力板或者开关来触发。

You should read about :wiki:`traps <trap>` and :wiki:`trap design
<trap_design>` on the wiki for a lot more detail, but only after you’ve
completed this tutorial, ok?! For now, get back to work!

你应该在Wiki上阅读 :wiki:`traps <trap>` 和 :wiki:`trap design
<trap_design>` ，以获得更多更加细节的信息，当然最好还是先看完咱们的这个教程，好不好？现在，让我们回到现在的工作中去！

* Scroll down to :guilabel:`Cage Trap` and hit :kbd:`Enter`.
* You now have a green X, place it in front of your entrance somewhere
  with :kbd:`Enter`.
* Do this a few times, you can see below how I’ve set a few traps to be
  built.

* 选择 :guilabel:`Cage Trap` （笼子陷阱），然后按下 :kbd:`Enter` 。
* 你的屏幕上应该出现了一个绿色的X，将它放在要塞入口的门口外面，然后按下 :kbd:`Enter` 。

.. image:: images/06-traps1.png
   :align: center

This is hardly an impressive defence, but it will do for now. With luck
it might help against any casual raiders, although sneaky 'trapavoid' monsters
will still be able to sneak past your traps. To beef our defence up a
bit we need to get some weapon traps installed as well as sort out some
way to strike back at attackers who aren’t so gracious to step on all
of our traps. So with that in mind, get producing a lot more mechanisms
(at least 10) and follow along as we produce some trap components!

这个防御决策看起来并不引人注目，但是它终将会发挥作用的。如果幸运的话，它会帮助我们抵抗一些糊里糊涂的入侵者，尽管一些机敏的怪物并不会因此上钩，而它们会直接从你的陷阱之上跨过去。为了再加强一点我们的防御工事，我们需要安装一些武器陷阱，同时也是为了反击那些并不仁慈的袭击者，让它们踩上所有的陷阱。所以要记住，生产更多的机械装置（至少10个），然后接下来继续生产制作陷阱的器件！

Smelting and other dwarfy things! 熔炼，以及其他矮人爱做的事情！
=================================
This wouldn’t be a *dwarf* fortress if we didn’t fill the place with
the clang of forge hammers and the stink of furnaces, right? Lets have
some fun then and make some sharp things to hurt our enemies!

如果我们不把里面放满叮叮当当的锻造锤，布置一堆味道奇怪的熔炉，这里怎么能叫做*矮人*要塞呢？让我们之后找点乐子，然后制作一些尖锐的玩意来刺穿我们的敌人！

.. note::

    If you haven't dug up any :guilabel:`Magentite` yet, this is a good
    time to go find it - it's a form of iron ore, and we can't smelt iron
    without any ore!

.. 注意::

    如果你还尚未挖掘出任何 :guilabel:`Magentite` （磁铁矿），现在是时候去找一些了 - 这是铁矿的一种，同时我们也无法在没有矿物的情况下熔炼铁器！

First up, let me point out that item production can be fairly
confusing. Essentially what we’re going to do is turn some wood into
charcoal (at a wood furnace), then use the charcoal to turn metal ores
into metal bars (at a smelter), then we’re going to use more charcoal
to smith metal bars into items (at a metalsmith’s forge).  As you can
no doubt imagine the problem we will face is the charcoal bottleneck.
It will take a lot of wood to fuel all our furnaces! Most of the time
this problem is got around by players using magma smelters and magma
forges. We haven’t struck magma yet, so we’re going to plough ahead
with the hard way, at least for a while.  The goal of all this is to
produce a few fearsome trap components to fit out some weapon traps –
think spinning disks from Indiana Jones! So follow along:

首先，我先说明一下，物品的生产可能是十分令人困惑的。一开始，我们要去做的事情是将一些木头制成木炭（在wood furnace，木炭炉里），之后用这些木炭烧炼矿物，将矿物制成金属锭（在smelter，熔炉里），然后我们要用更多的木炭将金属锭熔炼成各种物品（在metalsmith’s forge，金属锻造工坊中）。因此你会立马就想到我们要面对的，是木炭短缺的问题。想让所有的炉子烧起来，那么就需要很多很多的木材作为燃料！直到玩家能使用岩浆熔炉和岩浆锻造工坊之后，这个问题才能避开。我们并没有挖到岩浆，所以我们就只能走更加困难的路子——至少现在不得不走。我们做这些的目的，则是制作一些散发着恐怖气息的陷阱器件，装在一些武器陷阱上——想想（夺宝奇兵里）琼斯挥舞着刀片的那种感觉！所以接下来这样做：

* Find your wood furnace, hit :kbd:`q`, :kbd:`a`
* Select :guilabel:`Make Charcoal`, on :kbd:`r` repeat.
* With the job selected, :kbd:`Alt`-:kbd:`w` to open workflow.
  Set this to keep 5 to 10 charcoal bars available.
* Now go to the smelter, and try to :kbd:`a`, :guilabel:`Add new task`.
  You'll see a list of the tasks available at this workshop. Note, the tasks
  will change as you uncover more ores or deplete older ores. For
  interest, note that you can set the workshop to melt objects you wish
  to dispose of (turns the object back into a metal bar). More on that
  some other time.
* Chose :guilabel:`Smelt Magnetite Ore`, hit :kbd:`Enter`. Magnetite is
  a form of iron ore, so some of the rocks lying around will become iron bars.
* Fill the queue and then fill it again (or build another smelter and
  fill both queues).
* Once we’re done with this tutorial why not go and smelt all of that
  native platinum ore you saw on the list as well? If you’ve dug
  downstairs out you may have gold to smelt too!

* 找到你的木炭炉，按下 :kbd:`q`  :kbd:`a` 。
* 选择 :guilabel:`Make Charcoal` （制作木炭），按下 :kbd:`r` 重复任务。
* 选择任务后， :kbd:`Alt`-:kbd:`w` 来打开工作流菜单。将这个工作流设置为保持5到10单位木炭可用。
* 现在找到熔炉，按下 :kbd:`a` , :guilabel:`Add new task` （添加任务）。这时你就会看到工坊中可以激活的任务了。 注意，在你发现了更多的矿物或用尽了储备的某种矿物，那么这个任务列表会发生变化。为了利益的最大化，你可以让这个工坊熔炼任何你想要进行处理的东西（将一些物品熔炼回金属锭的状态）。之后会介绍更多的内容。
* 选择 :guilabel:`Smelt Magnetite Ore` （熔炼磁铁矿），按下 :kbd:`Enter` 。磁铁矿是铁矿的一种存在形式，所以散落在地上的一些矿石就会变成铁锭了。
* 填满整个队列，然后重复一次（或者建造另一个熔炉，重复一次动作）。
* 当我们完成了这个教程里的指引，为什么不去将任务列表里的铂金矿也一起熔炼呢？如果你继续向下挖掘，你也会找到一些金矿来熔炼！

Your dwarfs should be pretty busy now, hauling charcoal, wood, ores and
bars around. While you’re at it, make sure you’ve got enough wood around
for charcoal by cutting more down outside!

你的矮人现在应该十分忙碌，到处搬运木炭、木头、矿物和金属锭。当你忙于处理这些事务的时候，要确保你的木头存量一定要足够，也就是在外面要继续砍伐树木！

If you want to check your progress, go look at your Bar/Block stockpile
and see what is being built up. If you suspect all of your production
is in bins, use :kbd:`k`, find the bin, hit :kbd:`Enter` and look inside it.

如果你想要检查一下进度，你可以查看你的锭/块储物区，然后了解有什么正在产出。如果你觉得所有的产品都在箱子里，那就用 :kbd:`k` ，找到箱子，然后按下 :kbd:`Enter` 键来向里面查看。

初探食物处理
===========================
While all this is going on, why not go to your farmer's workshop and
add a task of :guilabel:`Process Plants (to bag)`. Set it on repeat.
Then add a task of :guilabel:`Process Plants (to barrel)`, again,
set it on repeat. Now some
dwarves will process anything you’ve grown that needs processing. I won’t
list those plants here now (go look at crops for all the detail), but
suffice to say, some plants need to be processed so they can be eaten
or used in other sorts of manufacture (eg, cloth making).

当这些事物正有条不紊地发展下去的时候，为什么不去看看你的农民工坊，然后添加一个 :guilabel:`Process Plants (to bag)` （处理植物（到包裹））的任务呢？将这个任务设置为重复任务。之后再添加一个 :guilabel:`Process Plants (to barrel)` （处理植物（到木桶）），然后再将这个任务设置为重复任务。现在一些矮人将会处理任何需要处理的成熟作物。我现在不会列出这些植物（查看作物来了解所有细节），但是可以说，一些植物需要进行处理，这样它们就可以进行食用了，或者是用于其他类型的工业制品（比如说，布料制作）。

Hopefully you’re not short on bags. If you are, try and pick up a bunch
off a trader. Alternately, buy leather and/or cloth off a trader and
using the Leather Workshop or Clothier’s Shop (we’ve not built one yet)
make bags. Bags are very handy for storing seeds and processed plants,
you can never have too many of them. They’re like barrels and bins in
that way.

希望你现在并不缺少包裹。如果缺少包裹的话，可以尝试从交易商队那里换一些包裹，或者，购买皮革或者布料，然后用皮革坊或衣物坊（我们现在还没有这个工坊）来制作包裹。包裹可以非常方便地储存种子和处理过的植物产品。包裹这东西，永远不嫌多；它们就像木桶和箱子一样。

I also just noticed that our farm entrance was entirely undefended,
so I added some doors and cage traps.  Hopefully that'll be enough!

我同样也注意到了我们的农场入口完全没有设置任何防御，所以我添加了一些门和笼子陷阱。希望这样做就已经足够了！

.. image:: images/06-traps-extra.png
   :align: center


回来炼铁！
=================
With a few iron bars made, head on back to the smelter - we’re ready
to rock! I hope, at this point, that you’ve not
been invaded, attacked or otherwise molested. With our fortress around
two years old now it’s likely you’ll start to see the odd goblin ambush
– especially if you send your dwarfs outside a lot. I hope you survive
those ambushes just fine! Perhaps we should have started this chapter
sooner...

做出了几块铁锭之后，回到熔炉那里去——我们准备大干一场！在这种关键时刻，我希望，咱们的要塞别被入侵、攻击，或者其他形式的干扰。我们的要塞已经发展两年了，现在你很可能会发觉到有一些古怪的哥布林隐藏在角落中，等待伏击——尤其是经常让矮人外出的你，可能早就发现了。我希望你的矮人们能活过这些伏击，并且还毫发无伤地回来！大概我们应该让这一章来得更早一些……

Anyway, head over to your metalsmith’s forge, and:
无论如何，来到你的金属熔炼工坊，然后：

* Hit :kbd:`q`, :kbd:`a` You will now see a list! Explore the list,
  see all the neat things we can make! You might need to hit :kbd:`Tab`
  to expand the menu size so you can see everything.
* Scroll to :guilabel:`Trap Components` and hit :kbd:`Enter`.
* Scroll to :guilabel:`Iron`, hit :kbd:`Enter`.
* Hit :kbd:`Tab`, :kbd:`Tab` so we can see the full item names.
* Chose to make a :guilabel:`large, serrated Iron disc`.
  I love the sound of that!
* Queue up three disks.

* 按下 :kbd:`q` ，  :kbd:`a` 键，你会看到一个列表。查看这个列表，看看我们所有能做的东西！你可能需要按下 :kbd:`Tab` 键来拓展整个菜单，以查看所有东西。
* 滚动到 :guilabel:`Trap Components` （陷阱器件），按下 :kbd:`Enter` 。
* 滚动到 :guilabel:`Iron` （铁质），按下 :kbd:`Enter` 。
* 按下 :kbd:`Tab` ，  :kbd:`Tab` ，由此我们就能看到完整的物品名称了。
* 选择并制作 :guilabel:`large, serrated Iron disc` （巨大的圆锯）。我很喜欢它所发出的声音！
* 安排队列制作三个圆锯。

At this point you might want to think about other items you’d like to
make in due course. Hit :kbd:`a` again and then hit space to back down the
make-item tree. Chose :guilabel:`Weapons and Ammunition`, chose
:guilabel:`Iron` and let's
make three Iron warhammers. These will come in handy once we start our
military. I like warhammers because it’s funny to watch goblins fly a
half dozen tiles and then expire in a heap.

在这时，你可能希望在适合的时候，制作其他物品试试。再次按下 :kbd:`a` 键，然后按下空格键，回到制作物品的树状列表中。选择 :guilabel:`Weapons and Ammunition` （武器和弹药），选择 :guilabel:`Iron` （铁质），然后制作三个铁质战锤。在我们搭建军队的时候，这些武器可是能派上大用处。看着哥布林被锤飞在半空中，然后摔落到地上，成堆地消逝，这可真有趣啊——这就是我钟爱战锤的原因。

Once you’re confident your items are made, go back and make sure you’ve
got some more charcoal in production, you’ve got more ore being smelted
and add, under Armor, three suits of iron chainmail and three iron
shields. This will all come in handy later.

只要你能确信物品都能建造妥当，然后再去确保一下木炭的产出也是正常的，你就可以熔炼更多的铁矿，然后回来锻造护甲，制作三套铁质链甲和三个铁质盾牌。这些装备等会儿能发挥很大的作用。

Oh, you may be getting annoyed at slow production. I know it’s been
bugging me. Perhaps it’s time to pack your production rooms with more
smelters and charcoal burners. One issue you’ll face is you need to make or
buy an anvil off the traders in order for you to make another metalsmith's
forge. Keep it in mind for your next trading deal.

哦，我猜你大概也是苦恼于生产缓慢的情况。我知道这种事情是十分让人困扰的。现在大概是时候清理一下生产房间，然后塞进去一些熔炉和木炭炉了。你大概会处理一个小问题，那就是需要搞到一个铁砧——要么自己做一个，要么从交易车队那里买一个。这样你就有条件建造另一个金属锻造工坊了。下次交易的时候可别忘了！

The other thing to consider is setting up Workflow for every step -
automating complex industries like steelmaking (or soap) is exactly
what it's designed for.

另外一件值得注意的事情，则是建立起一个自动处理的复杂工艺流程，比如说金属生产（或肥皂生产），这也是我们需要着力于去设计的地方。

Deploy Zee Traps!
=================
Once you’ve got your nice, big spikey discs, head back to your
entrance. Get ready for trap placing fun!

只要你做出来了精美的、巨大的圆锯，就回到咱们的入口处。来试试制作陷阱的乐趣吧！

* Go to place another trap, a :guilabel:`Weapon Trap` this time.
* Chose a mechanism.
* A new screen will be displayed looking something like this:

* 放置另一个陷阱，这次选择 :guilabel:`Weapon Trap` （武器陷阱）。
* 选择一个机械部件。
* 此时会显示一个新的界面，看起来就像下图一样：

.. image:: images/06-traps2-place.png
   :align: center

* From this screen you can chose the weapons you want to deploy with
  your trap. Facing goblins, we really only need one awesome iron disc
  per trap I think, but we’re able to select more if we wish.
* Scroll to the :guilabel:`large, serrated Ir...` and hit :kbd:`Enter`,
  you’ll see :guilabel:`1/3 selected`.  Weapon traps can have up to
  ten weapons each, but we need to save some for the other traps!
* Hit :kbd:`d` for :guilabel:`Done Selecting`.
* Your first weapon trap is now placed! Place three more.

* 在这个界面中，你可以选择放置在陷阱中的武器。面对哥布林的入侵，我觉得我们只需要放置一个铁质圆锯就足够了，但是如果想多放点，也不是不行。
* 滚动到 :guilabel:`large, serrated Ir...` （巨大的铁质圆锯），按下 :kbd:`Enter` 。
* 按下 :kbd:`d` ，即 :guilabel:`Done Selecting` （完成选择）。
* 你的第一个武器陷阱现在就放置完成了！然后再如法炮制地放三个。

Weapon traps are great ways of getting rid of a bunch of trash weapons
you might have lying around – this is particularly the case once you’ve
killed off a few goblins and have their rubbish cluttering up your
piles. When filling a trap with weapon trash you might want to put
ten low-quality weapons per trap, to ensure maximum damage!

对于清理无用的武器来说，武器陷阱是最好的选择之一——尤其是你清理掉一些哥布林后剩下的一堆杂乱无章的物品。在填满武器陷阱的时候，你可以在一个陷阱中放置十个品质低劣的武器，来让这个陷阱产生最大的伤害！

Here are my traps, laid out. I’m actually going to add another row of
slicey dicey weapon traps as I don’t have a military and I’m a bit
paranoid. Do something similar!

这是我布置的陷阱设计。实际上我还打算继续再安装一排类似的陷阱，因为我现在并没有训练军队，同时我还挺害怕外面的哥布林。你也可以试试看！

.. image:: images/06-traps3-types.png
   :align: center

Well done on the trap placing! But we’re not done with our defences
just yet. We need to stop our drunken citizens from casually strolling
into an invading army - and that means the Burrows system!

好样的，咱们的陷阱就布置好了！但是我们的防御工事依旧没有搭建好。我们需要阻止喝的烂醉的公民们不小心走近巡回的军队——而那就是通道系统！

Learn to love Burrows
=====================
"Wait!" you're probably thinking, "Why do we need filthy burrows when
we have a perfectly good fortress?".  Well, I'm glad you asked!

“等下！”你可能会想，“我们拥有一个如此完美的要塞，为什么我们还需要肮脏的地洞呢？”嗯，这个问题问得好啊！

A burrow isn't a physical part of your fortress, it's a way of designating
where dwarves are allowed to live and work.  More importantly, you can
set a :guilabel:`Civilian alert` to confine their movements too - perfect
for stopping Urist McSuicidal from wandering off to make friends with a
troll.  Here's how to set it up:

Burrow（地洞，这里的意思是通道）并不是我们要塞的物理组成成分，它的作用是指明矮人们能进行生活和工作的地方。更重要的是，你可以设置一个 :guilabel:`Civilian alert` （居民警报）来限制他们的行动——尤其是阻止那些喜欢游荡在外还喜欢和巨魔们交朋友的古怪矮子们。这里就是如何设置的介绍了：

.. image:: images/06-burrow.gif
   :align: center

Want some more detail?  You're in the right place! Start by hitting
:kbd:`w` for the burrows menu, and :kbd:`a` to add.  As shown in the
gif above, :kbd:`Enter` to designate is the important thing.
Cover the whole underground area, since it's safe there, and
:kbd:`n` name it something like "Inside".

想要更多的细节？你找对地方了！先按下 :kbd:`w` 找到通道菜单，然后按下 :kbd:`a` 键来添加。就像下面显示的图像一样，按下 :kbd:`Enter` 来设计。最后的回车确认是很重要的。覆盖整个地下区域，既然现在那里是安全的，那就按下 :kbd:`n` 来对该区域命名，比如说“Inside”（内部）。

.. image:: images/06-burrow-simple.png
   :align: center

The key feature here - for civilians, the military is complicated -
is that on the left we have :guilabel:`ALERTS` and on the right we have
:guilabel:`BURROWS`.  This means that we can set non-military dwarves to
be confined to a burrow, and change which if any that is with a single
alert setting.

关键特性就在这里——对于居民而言，军队是十分复杂的——在左侧我们有 :guilabel:`ALERTS` （警报），在右侧我们有 :guilabel:`BURROWS` （通道）。这就意味着我们可以设置非军队的矮人们限制在通道中，然后在需要的情况下，仅需一次警报的设置就可以更改通道状态了。

This is the :kbd:`m` military :kbd:`a` alerts screen, which is also
used for for civilian alerts.

下图是 :kbd:`m` 军队的 :kbd:`a` 警戒界面，同时也可以用于居民的警报。

.. image:: images/06-burrow-explanation.png
   :align: center

Now, let's set up a civilian alert in our new burrow, to make sure
nobody gets caught on the wrong side of the traps:

现在，让我们在新的通道中，设置一个居民警报，以确保没人在错误的位置上被陷阱捉住。

* :kbd:`c` to add an alert, then :kbd:`n` to name it.
  I called mine "Siege" because it's to use during a siege. Yep.
* Now move the cursor to the :guilabel:`BURROWS` column, and hit
  :kbd:`Enter` to associate that burrow with your new alert.
  You'll see a green :guilabel:`A` next to the burrow when you have
  that alert selected.
* Hit :kbd:`Esc` a couple of times to get out of the military menus.

* :kbd:`c` 来添加一个警报，然后按下 :kbd:`n` 键来对其进行命名。我称之为"Siege"（围攻），因为它会在围攻的时候使用。嗯，就是这样。
* 现在将光标移动到  :guilabel:`BURROWS` （通道）一行，然后按下 :kbd:`Enter` ，来将通道与新的警报关联起来。当你选择了这个警报，你会看到一个绿色的 :guilabel:`A` ，正对于通道的前面。
* 按下 :kbd:`Esc` 数次，来退出军队页面。

You're all set up!  In case of invasion :kbd:`m`, :kbd:`a`, select the
Siege alert, :kbd:`Enter`, and you should see a green :guilabel:`[CIV]`
next to it.

现在的防御工事已经完成了！为了防止入侵  :kbd:`m` ， :kbd:`a` ，选择“攻城警报”，按下 :kbd:`Enter` 键，然后你应该会看到在这个选项的旁边，会出现一个绿色的 :guilabel:`[CIV]` 。

Congratulations, your civilians are now confined to the burrow!
Watch any civilians who were outside drop what they were doing and come
running back inside. Note they will not go for food, so your safe room
may become a death sentence if there's no supplies (but that shouldn't
be a problem by now, right?).

恭喜，你的居民现在已经被限制在刚刚设置的通道中了！你可以看到你在外面的居民会扔掉手头的活计，然后跑回自己的要塞中。注意在这种情况下，他们不会外出觅食，所以如果没有补给的话，你的安全屋很有可能会变成一栋死亡监狱（但是现在这还并不是问题，对不对？）

When the 'fight' (against brave... traps) is over, come back and select
:guilabel:`Inactive` and hit :kbd:`Enter` to cancel the alert and let
your civilians back out of the burrow - that's all there is to it.

当“战斗”（对抗勇敢的……夹子）结束后，回来然后选择 :guilabel:`Inactive` （取消激活），然后按下 :kbd:`Enter` 键来取消警报，然后让你的居民们从通道中外出——这就是通道的用处了。

Anyway, that’s enough to now. Get on with building lots of traps,
making bags, armor, barrels, food, bins, shields, weapons and so on! Go
to it, and I'll see you in `the next chapter! <chapter07>`.

无论如何，做到现在这样已经足够了。继续建造一些陷阱，制作包裹、护甲、木桶、食物、木箱、盾牌、武器和其他物品！继续发展下去吧，我在 `下一章节 <chapter07>` 等你！
