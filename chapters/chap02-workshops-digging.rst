.. _chapter02:

################################
工坊、家具和楼梯
################################

Lets continue on with our exciting and fascinating Dwarf Fortress
Walkthrough! In this section we’ll work on getting some workshops set
up and we’ll dig out a lot more space for our shorties.  There’s lots
to do, so lets get cracking!

让我们继续我们激动人心的的矮人要塞之路吧！在着部分我们将会致力于建造一些工坊，与此同时我们也需要挖掘更多的空间来给这些工坊腾地方。现在可是有很多事情需要去做了，所以让我们尽情破坏吧！

建造工坊
===================
Workshops are a central feature of DF so lets get to grips with them
right away. Workshops are places your dwarfs work, usually turning base
materials into useful goods. The list of workshops can be a little
overwhelming (:kbd:`b` then :kbd:`w`), but don’t worry, we only need to
worry about a couple early on.

工坊是矮人要塞里的核心要素，所以就让我们现在就开始熟悉一下工坊的流程吧。工坊是矮人们工作的地方，往往能将一些原材料加工为有用的物资。工坊的列表可能有一点眼花缭乱（ :kbd:`b` 后按 :kbd:`w` ），但是不用担心，我们只需要为我们的早期发展操心就可以了。

First up we need a carpenter’s workshop, this workshop allows us to
turn wood logs into furniture and other items. You should see a lot of
wood scattered around in front of the fort, where the wood cutter has
left it after felling trees. We have no wood pile for it to be moved
to, you see.

第一件事，我们需要一个木工坊，这个工坊可以允许我们将原木加工为家具和其他物品。你可以看到很多的木头分散在要塞的前面，在砍完树之后我们的伐木工已经离开了。可以看到，我们现在还没有能让他搬运的木头。

Ideally, we’d like to build the carpenter’s workshop inside, but we
haven’t dug a nice enough space for the workshop yet, so lets chuck it
outside for now:

在理想的布局中，我们希望能将木工坊建立在我们的要塞内部，但是我们目前还没有挖掘足够大的空间来放置木工坊，所以我们需要将这个工坊放在外面：

* Move the view to the front of the fortress.
* Hit :kbd:`b` for :guilabel:`Building`, and then :kbd:`w` for
  :guilabel:`Workshops`. You can also just scroll down to
  :guilabel:`Workshops` using :kbd:`=` and hit :kbd:`Enter`.
* You will now see a list of workshops. It scrolls off the bottom of
  the page (scroll around if you like)! Hit :kbd:`c` for
  :guilabel:`Carpenters workshop`.
* The menu will vanish and the placement menu will appear. The green
  X’s mark out the workshop’s footprint. The dark green Xs are squares
  that will be impassible once the workshop is built, keep this in mind
  when building in an enclosed space, you don’t want to block the door!

* 将视角移动到要塞的门口。
* 按下 :kbd:`b` 键切换到 :guilabel:`Building` （建造）选项，之后按下 :kbd:`w` 键切换到 :guilabel:`Workshops` （工坊）。你同样也可以使用 :kbd:`=` 键和 :kbd:`Enter` 键来切换到 :guilabel:`Workshops` （工坊）。It scrolls off the bottom of the page（如果你喜欢的话，可以滚动这个列表看一下其他内容）！按下 :kbd:`c` 键选中 :guilabel:`Carpenters workshop` （木工坊）。

.. image:: images/02-carpenter-place.png
   :align: center

* Chose a spot in front of the fortress. Somewhere around where mine is
  marked should do. Once you’re ready to place your workshop hit
  :kbd:`Enter`.
* The menu on the right will now change to a list of items you could
  use to build the workshop with. Mostly it will be a list of wood and it
  will look something like this:

* 选择要塞内部的一个地方，可以放在标记矿工需要挖掘的地方。只要你决定好放置的工坊位置，按下 :kbd:`Enter` 来确定。
* 右侧的菜单现在将会切换为你可以用来建造工坊的材料。在绝大多数情况下，这个列表会是一个含有各类木头的列表，就像下图所示：

.. image:: images/02-carpenter-material.png
   :align: center

* At this point you could just hit enter and the first item on the list
  will be used to build the workshop. Instead, for fun, hit :kbd:`x` and
  :guilabel:`Expand` the view to show a list of every single item the workshop
  can be built from. Expanding the item selection is a nice way to chose
  exactly the item you want to be used. Most of the time you can ignore
  this option, but it will come in handy later on.
* Close the expanded menu with :kbd:`x` again. For your interest note
  that items are sorted by distance from the workshop’s current location.
  Usually you want to build with the closest items to save your dwarfs a
  slow walk.
* Once you’ve hit enter (selecting the building material) the workshop
  will be placed in an un-built state on the map. It will look like this:
  
* 此时，你可以按下回车键，列表里的第一个选中的物品就会被用来建造工坊。此外，如果感兴趣的话，按下 :kbd:`x` 键同时 :guilabel:`Expand` （展开）视图，就可以显示所有可以被用来建造工坊的单个材料。如果你希望去选择特定的物品来使用，展开物品选择是十分推荐的一种方式。但绝大多书情况下你可以忽略掉这个选项，但是在之后的操作中是一种方便的捷径。
* 再次按下 :kbd:`x` 后就可以关掉展开的菜单。这里需要注意一下，物品都是按照离工坊所在的距离来分组的。一般情况下你还是希望使用离工坊最近的物品，以节约矮人们搬运材料的路程。
* 只要你按下回车键（选择一个建筑材料），工坊就会被放置在地图上，状态显示为尚未建造，如下图所示：

.. image:: images/02-carpenter-template.png
   :align: center

Your workshop is now about to be built. You will have to hit :kbd:`Esc` a
couple of times to un-pause the game, but as soon as you do I bet you
that a helpful wee dwarf will run over and start building it. If you’re
quick you can hit :kbd:`q` and see the status of the workshop, it
should say :guilabel:`Construction initiated.`.

你的工坊现在就已经在准备建造了。你应该会几次按下 :kbd:`Esc` 来让游戏继续运行，但是只要你这样做了，我能保证你可以看到一个善于帮助的小矮人跑过去，然后开始建造工坊。如果你足够快的话，你可以按下 :kbd:`q` 键来检查工坊的状态，这种状态被称为  :guilabel:`Construction initiated.` （建造物已初始化）。

A few seconds later, the workshop is built!

几秒钟后，你会看到你的工坊已经建立起来了！

.. image:: images/02-carpenter-built.png
   :align: center

It doesn’t look like much, but it’s really useful! I promise! So lets
get turning some of this lumber into stuff our dwarfs can use. Just
don’t tell the Elves, ok? They really hate us chopping down trees.
Pointy-eared gits, a magma-steam death is too good for them!

这看起来并没啥意义，但是这个的用处可大了！我可以拍着胸脯说！所以让我们将一些原木材料加工为我们矮人可以使用的东西。千万别告诉精灵族，好吗？它们可是非常憎恶我们砍树的行为。尖耳朵的废物，最好的归宿就是葬身岩浆！

来几张像样的床
===========
Dwarfs don’t like sleeping in the dirt, they like nice, comfy beds -
and solid gold statues in their bedrooms, but those will come later.

矮人们不喜欢睡在冰冷的泥土地上，他们喜欢精美的、舒适的床，还有卧室里的金色雕像——这点我们稍后再提。

Follow along with me:

跟我这样一起做：

* Hit :kbd:`q` and move the cursor over the carpentry workshop.
* The menu on the right now shows you the actions you can take,
  specifically, we’re interested in :kbd:`a` :guilabel:`Add new task`.
* Hit :kbd:`a` (duh)!
* Note the right menu is now a large list of stuff we can make. We’re
  after beds.
* You can scroll down to beds using :kbd:`=`, or simply hit :kbd:`b`.
* The menu now changes back to the :kbd:`q` list and you’ll see
  :guilabel:`Construct bed` listed. But we want more than one bed!
* Hit :kbd:`a` again, and then :kbd:`b`. Repeat until we’ve got five
  beds up there. We don’t need a bed for every dwarf just yet, luckily
  for us they seem happy to sleep in shifts.

* 按下 :kbd:`q` 键，将光标移动到木工坊。
* 现在右侧的菜单会显示你现在可以进行的动作。具体来说，我们对 :kbd:`a` :guilabel:`Add new task` （新建任务）十分感兴趣。
* 按下 :kbd:`a` (啊哒)！
* 注意右侧的菜单，现在显示了我们目前所有可以制作的物品。我们需要找到床。
* 你可以用 :kbd:`=` 键向下滚动来找到床，也可以按下 :kbd:`b` 键直接选择。
* 现在的菜单将会变回 :kbd:`q` 键显示的那个列表，你会看到 :guilabel:`Construct bed` （建造床）这个选项。但是我们不仅要一张床！
* 再次按下 :kbd:`a` 键，之后我们按下 :kbd:`b` 键。重复这个动作，直到我们可以看到有五张床显示出来。我们目前还不需要为所有的矮人都准备一张床，幸运的是他们看起来还是比较喜欢轮班睡觉的。

Your carpentry workshop menu should now look like this:

你的木工坊菜单现在应该长得像这样：

.. image:: images/02-carpenter-menu.png
   :align: center

If your workshop menu looks like this, well done! Five beds are queued
up and hitting :kbd:`Esc` a couple of times will un-pause the game and set a
dwarf working, building your beds. You may have noted that there are a
lot of other workshop options available to us now we have items on the
build list. We can :kbd:`s` suspend any construction, set any item to
be :kbd:`r`, repeat built, or we can hit :kbd:`c` and cancel the
construction of the current listed item. We can also have the entire workshop
removed using :kbd:`x`. For now, lets just watch the beds get built!

如果你的工坊菜单看起来就像上面一样，真棒！制作五张床将会被分配到任务队列中，之后几次按下 :kbd:`Esc` 键，让游戏继续运行，让矮人过去工作，制作你需要的床。你应该能注意到现在还有其他很多工坊选项可供我们选择。我们可以按下 :kbd:`s` 来推迟一些结构的任务，或者设置一些物品来 :kbd:`r` ，重复建造，或者我们可以按下 :kbd:`c` 键来取消这个工坊的任务。当然，我们可以用 :kbd:`x` 键来直接移除掉这个工坊。现在，就让我们静静地看着床已经在建造当中了！

Note, you won’t see the beds being put anywhere. Want to know why? The
answer is simple, and if you’re extra-special clever you may have
already worked it out: We have no furniture stockpile! Lets fix that:

注意一下，你并没有看到床被安置在任何的地方。想知道为什么吗？答案很简单，如果你足够聪明的话，你应该早就能够明白这个的原因了：我们现在还没有家具库存区！让我们来修正一下吧：

* Hit :kbd:`p` from the main menu.
* hit :kbd:`u` for :guilabel:`Furniture Storage`.
* Move the X near the workshop, hit :kbd:`Enter`, and designate a pile about 5
  tiles by 5 tiles.

* 在主菜单按下 :kbd:`p` 键。
* 按下 :kbd:`u` 键来选择 :guilabel:`Furniture Storage` （家具存储）。
* 将光标移动到工坊附近，按下 :kbd:`Enter` 键，之后指定一个5x5的区域。

Here’s my pile after a few seconds of the game resumed:

在游戏继续运行的几秒钟后，这就是我所指定的区域：

.. image:: images/02-pile-furniture.png
   :align: center

You’ll note the dwarfs have moved the constructed beds to the pile as
well as some spare barrels, our anvil and a bag. As stated above, we
don’t usually like to leave our stuff lying around outside, even in
tidy piles, but as a temporary solution it helps keep things organised
and gets the furniture moved from the workshop. If items aren’t pulled
out from under the carpenter’s feet then eventually the workshop will
get cluttered (marked with :guilabel:`[CLT]` when you :kbd:`q` over the
workshop) and a cluttered workshop works slower.

你会注意到矮人们开始移动将已经做好的床移动到刚才我们指定的区域，其他的空桶、铁砧和包裹都被移过来了。就像我们刚开始的那样，我们通常并不希望让我们的物品散落在外面，尽管这些物品都排放整齐，但是作为一个临时的解决方案，它能让事情变得更加有组织，也可以让家具从工坊里移动出来。如果物品没有被及时地从木工手里移动出来，那么最终的话工坊将会变得杂乱无章（当你在工坊的上面按下 :kbd:`q` 键，那么会被标记为 :guilabel:`[CLT]` ），如果一个工坊变得杂乱无章的话，那么工坊的运作效率将会变低。

Later on we can remove piles by using :kbd:`p` and then :kbd:`x` and
selecting the whole area of the stockpile. What’s better is that
helpful dwarfs will grab those items and move them to another suitable
pile. But enough of that, lets get on with building our fortress!

之后我们可以通过先后按下 :kbd:`p` 键和 :kbd:`x` 键，选择整个库存区，来取消库存区的设置。这么做的好处是乐于助人的矮人们将会拿去这些物品，并且将这些物品放置在其他合适的库存区。但是我们现在玩够了，我们可以继续来扩充我们的要塞！

挖掘，不择手段地挖掘！
=========================
What kind of Dwarfs would we be if we only scratched around on the top
of this mound like dirty hobbits? We want to dig down, down, down! The
easiest way of doing this is to build some stairs. Well, to be precise,
we’re going to designate some stairs using the :kbd:`d` menu. We are
going to designate some un-dug space to be stairs and then see what’s
going on a few metres down. So follow along, and lets get expanding!

如果我们像脏兮兮的霍比特人，仅仅在这个小土堆的顶上刨土，那么我们矮人们将会是什么样子呢？我们需要向下挖掘，不择手段地挖掘！而达到这个目的最简单的方式是建造一些楼梯。好，更加准确地说，我们需要使用 :kbd:`d` 键的菜单里的某些项目来设计一些楼梯。我们会去指定一些没有被挖开的空间来成为楼梯，之后来观察向下挖掘几米后的变化。所以就跟着这里的只是，让我们扩充要塞吧！

* Move the map to the top side of our corridor.
* Hit :kbd:`d` and then :kbd:`j` for :guilabel:`downward stairway`.
* Move the X into the black and hit enter, move the cursor down one,
  and hit enter again.

* 将地图视图移动到我们走廊的顶端。
* 按下 :kbd:`d` 键，之后按下 :kbd:`j` ，代表着 :guilabel:`downward stairway` （向下的楼梯）。
* 将光标移动到黑色的地方，之后按下回车键，将光标向下移动一格，再次按下回车键。

You should see six downwards stairs marked.
You may be wondering why I have set six stairs down. The reason is that
dwarfs need space to move past each other. If the corridor, or stairs,
are only 1 wide, then they have to pause to let each other pass. This
slows movement around your fortress down terribly once you have dozens
of dwarfs running around. Six stairs will be plenty for a long time.

你应该可以看到有六个向下的楼梯被标记了。你可能会好奇，啊为什么我会设置六个向下的楼梯。原因很简单，矮人们需要空间来移动。如果走廊或是楼梯，只有一格宽，那么他们不得不停下来，让其他人经过。一旦你的要塞里有二三十个矮人跑来跑去，那么你的要塞的运作效率将会低到一个可怕的水平。六个楼梯将能应付很长一段时间。

Hit space until the game is running and let that area get built. It
should look something like this when it’s done:

按下空格，直到游戏继续运行，然后等待区域被建立。当它被建造完成后，它看起来应该像这样子：

.. image:: images/02-stairs-down.png
   :align: center

Super! A hallway and two downward staircases! At this point you may be
feeling pretty chuffed with yourself. But if you go down a level
(:kbd:`>`, you’ll recall) you’ll not see any stairs going up, or much
of anything, just these tan blobs:

壮哉！一个走廊和两条向下的楼梯！这时你可能会感到很愉悦很有成就感。但是如果你向下一层的话（ :kbd:`>` ，你应该还能记得吧），你会注意到没有向上的楼梯，或者换句话说，有很多的东西，就是这些棕褐色的斑点：

.. image:: images/02-stairs-lower.png
   :align: center

Where are our stairs going up? Where are our vast stores of mineral
wealth? Well, this is where stairs get crazy-strange. Let me explain…

我们向上的楼梯呢？我们那丰盈的矿物资产呢？别急，这就是楼梯的建设十分奇怪的原因所在。让我解释一下……

To dwarves a down stair is just an exploratory stair dug down to see
what’s on the level below. It’s like knocking a hole in the floor so
you can look down on your neighbours in the flat below, hoping perhaps
to work out what the weird noises are.

对于矮人来说，一个向下的楼梯就只是一个探索性的楼梯，向下挖一下，来看看这层楼梯之下的内容。这就像是在公寓的地板上敲开一个洞，所以你可以向下看到你楼下的哥们在干嘛，希望想找到奇怪的噪音到底源于何处。

To actually go down to the level below, you need to build stairs going
up from the level you’re digging to. Or in our case, we want to build
an :guilabel:`Up/Down Stairway` as we are looking to dig down a long,
long way. An Up/Down stairway pokes the proverbial holes in both
directions. It links up with a stairway above (if there’s one there)
and tries to link up with a stairway below (if there’s one there). If
there’s no stairway to link up with it will provide access to those
other levels so you can build the needed stairs.

为了真正地到达下面的一层，你需要在你所希望要向下挖掘的地方，向上一层搭建楼梯。或者就拿我们这个情况来说，我们想要建造一个 :guilabel:`Up/Down Stairway` （上行/下行楼梯），因为我们想去向下挖掘一条长长的路。一个上下楼梯会连通上下两层的方向。它连接了上层的楼梯（如果有的话），同样也会去尝试连接下层的楼梯（如果有的话）。如果没有需要连接的楼梯的话，那么这个楼梯将会提供一条通路，以方便你去建造这些必须的楼梯。

Here's a handy side-on diagram:

这里就是一个比较通俗易懂的图表：

.. image:: images/stairs-diagram.png
   :align: center

To build an up/down stairway, do this:

想要去建立一个上/下楼梯的话，这样做：

* Go down one level from our main area.
* At this level, below our down stairs, hit :kbd:`d`, :kbd:`i`
  to designate an :guilabel:`Up/Down Stairway`.
* Move the X over the tan blocks. Hit :kbd:`Enter`, move down one level,
  and hit :kbd:`Enter` again.
* Brown X’s now show that your up-down stairway has been designated.
* If you mess it up, :kbd:`d` and then :kbd:`x` can un-designate the
  space for you.
* Resume the game and the up/down stairs will be dug. The designation
  looks like this:

* 从我们的主区域向下一层。
* 在这一层，就是在刚才我们的建造向下楼梯的下一层，按下 :kbd:`d` ， :kbd:`i` 来设计一个 :guilabel:`Up/Down Stairway` （上行/下行楼梯）。
* 将光标移动到棕褐色的方块上。按下 :kbd:`Enter` ，向下一层，再次按下 :kbd:`Enter` 键。
* 棕色的X现在可以指示你的上行/下行楼梯已经规划完毕。
* 如果你不小心搞错了。你可以按下 :kbd:`d` 之后按下 :kbd:`x` ，移除不需要的规划区域。
* 继续游戏，之后上行/下行楼梯就会被挖掘出来。这里的设计看起来就像下图所示：

.. image:: images/02-stairs-updown.png
   :align: center

Lovely! Perfect up-down stairs. But we’re not done yet.

真可爱！完美的上行/下行楼梯。但是我们现在还没完成。

So far you've been selecting two corners to designate a rectangle in
two dimensions - but laying out a staircase would get pretty tedious
like that!  Luckily DF supports three-dimensional designations as well,
so lay it out in 2D, then :kbd:`>` to level 103 to finish the column.

到目前为止你已经选择了两个角，去在一个二位的平面上来进行结构的设计——但是这样制定一个楼梯将会十分单调乏味！幸运的是，矮人要塞同样也支持三维设计。所以我们可以在2D平面上进行制定，之后按下 :kbd:`>` ，直到第103层来完成最后的列。

Rock! We’ve got some rock around us! This is the sort of place a good
dwarf loves! How about we dig out some of this space? I suggest a set
of square rooms just off the staircase, with two tile wide doorways.
It's a simple layout, but enough for now. See if you can match mine.

岩石！我们的周围都是岩石！这种地方可是矮人们十分喜爱的地方！我们在这个地方挖出一片地方来怎么样？我建议你可以设计一些方形的房间，就在楼梯的旁边，之后放两格宽的大门。这算是一种简单的设计，但是就现在而言是足够的。来看看你能不能做得和我一样：

.. image:: images/02-rooms-large.png
   :align: center

Now let your boys dig! Dig boys, dig! And make sure all your areas set
to be dug connect back to your stairs!

现在就让你的伙计们挖掘吧！挖掘小子，快挖！同样也需要确保你所有的区域要和你的楼梯相连通！

Along the way you may get various messages about striking various ores.
That’s good news! Metal is very handy, as you can imagine. But we’ll
worry about metal later.

在向下挖掘的路上，你可能收到了一些消息，比如说挖到了不同种类的矿石。这可是个很棒的消息！就像你想象的那样，金属可是随处可见的。但是我们待会儿再来谈谈金属。

地上的玩意是啥？
==========================
While they’re busy digging, why not have a look at what is scattered
all over the ground. To do that, from the main menu, hit :kbd:`k` to
look around. An X will appear and you can direct it using
the arrow keys. As you move it around you’ll see what’s under the
arrow. Check my example:

在他们辛勤挖掘的时候，为什么不去看看地上多出了一些什么东西呐。为了做到这一点，在主菜单，按下 :kbd:`k` 键来观察周围。一个光标将会出现，之后你可以用方向键来控制光标。在移动光标的时候，你可以看到该光标指向的地方有什么东西。就以自己的为例子吧：

.. image:: images/02-look-around.png
   :align: center

As you can see, underneath my X is a dwarf, some limestone, and a
limestone cavern floor. I can use :kbd:`=` to scroll down
through those three items, and hitting :kbd:`Enter` will display me some info
about each. Ok, except for the floor. I mean, what do you want to know?
It’s a floor!

正如你所看到的这般，在我的光标下面有一个矮人，有一些石灰岩，以及石灰岩洞穴地板。我可以用 :kbd:`=` 键来滚动切换这三个物品，在之后按下 :kbd:`Enter` 键，就会显示每个物品的详细信息。OK，当然除了地板。我说，你还像知道什么？那仅仅就是一个地板！

:guilabel:`Look around` is a very handy way to see what is in a square.
Sometimes items get stacked a few deep and you’re not sure what is
what, :kbd:`k` will show you. Also, it is very handy way to find out
what the walls and ground are made of. For example, without irrigation
you won’t be able to build a farm plot inside on anything but soil,
sand, silt, clay or loam. :guilabel:`Look around` will show you what
the ground is made of.

如果想查看一个区域的物品， :guilabel:`Look around` （查看周围）选项是一个十分方便的方法。有时候物品会叠成一摞，你也不知道这一摞东西到底是啥，按下 :kbd:`k` 键就会告诉你了。当然，这种方式也可以十分轻松地地制墙面和地板是由什么制成的。举个例子，如果没有灌溉的话，你只能在泥土、沙子、淤泥、粘土和壤土上建立农场地块，其他的不行。 :guilabel:`Look around` （查看周围）选项能够显示这里的地面是由什么材料构成的。

Toy with :kbd:`k` for a while, and see what minerals are being dug up.
Then lets leave the dwarfs digging, we’ve got a very important job to do...

玩了一阵子 :kbd:`k` 键，然后观察矿物被挖掘出来，之后就让矮人们继续挖掘吧，我们还有一件十分重要的事情要去做……

把垃圾扔出去！
=======================
By now, believe it or not, some rubbish may be building up in your
fortress. I’ve taken a shot of some rubbish near our food pile. Have a
look at those slimy bones in with the barrels of booze:

现在，无论你是信还是不信，有一些垃圾正堆积在你的要塞之中。我在食物库存的附近截了张图，可以看到已经产生了垃圾。让我们来看看那些附着在酒桶上的黏糊糊的骨头吧。

.. image:: images/02-food-trash.png
   :align: center

Using :kbd:`k` I can have a closer look and see exactly what this trash
is... :guilabel:`Rat remains`. Yuck!
We can’t have this left lying around our tidy fortress,
can we? Well, we could, but the end result would be a stinking cloud of
purple miasma. Miasma makes our dwarfs unhappy. While they may never
shower and probably reek to high-heaven, they really don’t like the
smell of rotting leftovers. Go figure.

按下 :kbd:`k` 键，我们可以更加详细地看这个垃圾到底是个啥…… :guilabel:`Rat remains` （老鼠残留物）。D区！我们可不能让这种货色留在干净整洁的要塞中，对不对？好，我们当然可以留下，但是最终的后果则是产生一阵阵臭气熏天的紫色瘴气，而这瘴气则让我们的矮人们不高兴。由于我们的矮人们并不会洗澡，也可能会被臭气薰到原地升天，他么可不喜欢这些残渣。你想想看。

So how do we get rid of the refuse? Easy! We build :kbd:`p`
:guilabel:`Stockpiles` for :kbd:`r` :guilabel:`Refuse`. But note, we
need to build this pile outside or we’ll get miasma build-up. Outside,
the wind blows the stink away.

所以我们是如何去清理这些垃圾呢？很简单！我们可以按 :kbd:`p` 键（ :guilabel:`Stockpiles` ，库存），再按下 :kbd:`r` 键来建立 :guilabel:`Refuse`
 （垃圾）库存。但是需要注意的是，我们需要将这个库存建立在要塞之外，否则瘴气将会在要塞的内部生成。建在要塞之外，风会将这恶臭的气味吹走。
See if you can set up a refuse pile on your own like mine. It’s just
like making any other pile, except we set :kbd:`r` for refuse as we
plot it.

来看看自己能不能按自己的喜好建立一个垃圾库存吧。这步骤就像建立其他库存的方式相同，除了需要按下 :kbd:`r` 键来建立垃圾库存，就像我们所希望的那样。

.. image:: images/02-pile-refuse.png
   :align: center

Look, some of the refuse has already been moved!
Phew, miasma crisis averted!

看，一些垃圾已经被移出来啦！啧，瘴气也跟着被转移啦！

But we still have one really, really important job to do…

但是我们依然有已经十分重要的事情去做……

酿酒！
=============
Booze is the lifeblood of dwarven society. Literally so. Dwarfs, unless
injured, will prefer to drink booze over water 100% of the time. If
there is no booze, they will drink water, and that's terrible - they work
slower, and are more prone to murderous tantrums.

酒是维持矮人社会运作的血液。你完全可以按照字面意思来理解。矮人，除了受伤的之外，在所有的情况下都会优先饮用酒品，而不是水。如果没有酒的话，他们就会饮用水，但是那很糟糕——他们的工作速度将会变慢，而且更加有可能会变得暴脾气、发怒。

So what do we do about the dwarf booze demands? Simple! We build a still!

所以我们该做什么来满足矮人们饮酒的需求？简单！我们来建造一间蒸馏坊！

Follow along, dear reader:

亲爱的读者，跟着下面的步骤来做吧：

* Hit :kbd:`b` for :guilabel:`Build`.
* Hit :kbd:`w` for :guilabel:`Workshops`.
* Hit :kbd:`l` for :guilabel:`Still`, or scroll through and find the
  still yourself, and hit enter.
* Place the still in the room above the food stockpile, like this.

* 按下 :kbd:`b` 键打开 :guilabel:`Build` （建造）菜单。
* 按下 :kbd:`w` 键打开 :guilabel:`Workshops` （工坊）菜单。
* 按下 :kbd:`l` 键打开 :guilabel:`Still` （蒸馏坊）菜单，或者通过滚动可选项来找到蒸馏坊，之后按下回车键。
* 在食物库存房间的隔壁放置一个蒸馏坊，就像下图所示：

.. image:: images/02-still-place.png
   :align: center

Placing it near food makes sense, as it will turn food items into
booze. The less walking for our brewer, the better. Once you’ve placed
it and selected the materials (any will do), your still should be
quickly built and look something like this:

把蒸馏坊放置在食物库存的旁边，这很合理。因为蒸馏坊可以将一些食物转化为酒。我们的酿酒师少走点路，效率就会提高。一旦你放置了蒸馏坊，之后选择了建造用的材料（有些人将会做），你的蒸馏坊会很快地建造完成，而且看起来就像下图一样：

.. image:: images/02-still-built.png
   :align: center

Before we brew some booze, there's one more thing we need: barrels.

在酿造酒品之前，还有一件事情需要去做，那就是制作木桶。

* Head to the carpenter.
* Hit :kbd:`q`.
* Hit :kbd:`a`.
* Scroll down to :guilabel:`Make wooden Barrel`, or just hit :kbd:`v`.
* Fill the queue with barrels.

* 找到木工坊。
* 按下 :kbd:`q` 键。
* 按下 :kbd:`a` 键。
* 向下翻到 :guilabel:`Make wooden Barrel` （制作木桶），或者按下 :kbd:`v` 键。
* 将队列里填满木桶。

Now to make it start churning out the brewskies!

现在是时候去开始大量生产酒精饮料了！

* Hit :kbd:`q` and move the cursor to the still.
* Hit :kbd:`a` for :guilabel:`Add new task`.
* Hit :kbd:`b` for :guilabel:`Brew drink from plant`
* Hit :kbd:`a` and :kbd:`b` another 7 or so times.

* 按下 :kbd:`q` 之后将光标移动到蒸馏坊。
* 按下 :kbd:`a` 选择 :guilabel:`Add new task` （添加新任务）。
* 按下 :kbd:`b` 选择 :guilabel:`Brew drink from plant` （从植物中酿造饮品）。
* 按下 :kbd:`a` 之后按 :kbd:`b` 键7次或更多次。

Yay! You have now queued up a lot of beer to be made.

好耶！你已经添加了很多啤酒制作任务队列啦。

To keep booze production at an acceptable rate, there's a couple of
options.  One is to keep checking manually, but you only have to be
late once to cause a tantrum!  The second is to set up repeating jobs
(:kbd:`q`, select job, :kbd:`r`), but then it's easy for production
of drinks and barrels to get out of sync and waste valuable materials.

为了能让酒类生产能够保持在一个可以接受的速率上，有很多可以修改的设定。其中之一则是手动去检查，但是一旦矮人们发了脾气，你就知道你已经晚了。第二种方法则是设置循环工作（ :kbd:`q` 键，指定工作， :kbd:`r` ，重复工作），但是之后饮料的生产和木桶的生产将会轻易地失去同步，然后浪费有价值的原材料。

The third option is a plugin called ``workflow``, which lets you set a
target amount and will suspend and unsuspend production jobs to keep you
at that level.  :kbd:`q`, select job, :kbd:`Alt`-:kbd:`w`, :kbd:`A` to
:guilabel:`Add limit`, :kbd:`R` adjust the range.  This might seem
complicated, but setting 50-100 drinks and 10-20 barrels will keep things
flowing smoothly until your population is a lot larger.

第三种方式则是使用一种插件，叫做 ``workflow`` （工作流），它可以让你设置一个目标数量，同时能够暂停或继续生产工作，让产出物的水平保持在你设置的数量水平上。 :kbd:`q` 键选择工作， :kbd:`Alt`-:kbd:`w` ，:kbd:`A` 键来选择 :guilabel:`Add limit` （添加限制）， :kbd:`R` 来调整范围。这个理解起来可能有一些复杂，但是设置50-100的饮品和10-20个桶将会保持物品流平滑过渡，直到你的人口数量更加庞大。

End result, happy dwarfs! An important thing to note at this point is that
brewing alcohol uses plants, but doesn’t destroy the seeds. Cooking
does destroy the seeds of any plant cooked, which can seriously
compromise your ability to grow more food! So for now, lets just stick
to making some extra booze. We should be ok for food for a while yet
with our farm running, since dwarves love raw mushrooms.

最终的结果，那就是快乐的矮人啦！一个需要重点注意的事情是，在这种情况下，酿造酒精饮料会使用植物，但并不会破坏种子。但是烹饪则会破坏掉用来烹饪植物的种子，有时候可能会让你达不到继续扩大种植的目标！所以到现在为止，让我们继续去制作一些额外的酒。由于现在的农场还在运作，我们现在对于食物的供应上还可以，而且矮人们也喜欢生的蘑菇。

Time to head back down stairs to see how our miners are going. Let them
dig at least one room out before continuing with the next step.
Hopefully they’re not sleeping on the job (The big :guilabel:`Z`
flashing on them)!

是时候来顺着向下的楼梯开看看我们的矿工们干的怎么样了。在下一步继续之前，让他们先挖出来至少一个房间。希望他们不要在干活的时候睡觉（一个大大的 :guilabel:`Z` 符号在他的图标上闪烁）！

门的世界！
===================
We need doors. Lots of doors. We need beds, doors, chairs, tables. We
need lots of them. If you’re starting to feel like an Ikea salesman,
don’t worry, when you see a bed menacing with spikes of cat leather,
engraved with an image of a dwarf striking down a goblin with other
dwarfs laughing, then you’ll know you’re no longer in Sweden. So how
are we going to get all of this wonderful furniture?

我们需要门。很多很多的门。我们需要床、门、椅子、桌子，需要很多很多。如果你觉得你现在就像一个宜家销售员的话，不用担心，当你发现你的床是由猫皮钉在上面的床，上面还刻着一个雕刻画，画着一个矮人击倒了一个哥布林，旁边还有其他矮人们开怀大笑，你就明白其实你并不在瑞典（宜家是一个瑞典的家具品牌）。所以我们该如何获得所有这些“精美”的家具呢？

Well, we could build some doors and assorted other stuff in our
carpenters’ workshop. But that would use valuable wood. Much better
would be to use all that stone you can see strewn about. So how do we
do that? I’m glad you asked! We build a masons workshop. Here’s how:

好，我们可以在我们的木工坊里制作一些门和其他各种各样的玩意。但是这样做的话会使用相当珍贵的原木。更好的方式是使用石头，你会发现它散落在要塞的地上。所以我们到底该如何去做呢？很高兴你能问出这个问题！我们将会制作一个石匠坊。下面是流程：

* Hit :kbd:`b` for :guilabel:`Build`.
* :kbd:`w` for :guilabel:`Workshops`.
* :kbd:`m` for :guilabel:`Masons Workshop`.

* 按下 :kbd:`b` 键切换到 :guilabel:`Build` （建筑）。
* :kbd:`w` 切换到 :guilabel:`Workshops` （工坊）。
* :kbd:`m` 切换到 :guilabel:`Masons Workshop` （石匠坊）。

Chose an area near our main stairs up, in one of our new rooms. You can
check the shot below to see where I’m going to place mine.

选择离主楼梯间较近的位置，将石匠坊放在我们的新房间里。你可以看一下下面的截图，参考一下我放置的位置。

.. image:: images/02-mason-where.png
   :align: center

* Select some stone and get the thing built!

* 选择一些石头，然后让工坊开始建造！

Once it’s up it’s time to get it producing some useful items:

一旦石匠坊建立完毕，是时候让石匠坊来生产一些有用的物资了：

* :kbd:`q` over the Mason's Workshop.
* :kbd:`a` for :guilabel:`Add new task`, then :kbd:`d` for door. Do this
  four times.
* :kbd:`a` for add task, then :kbd:`t` for table, do this twice.
* :kbd:`a` for add task, then :kbd:`c` for chair, do this twice.
  Note that in DF, a chair made from stone is called a :guilabel:`throne`.
  They're not the only thing to have different names depending on the
  material, so if in doubt check the wiki.

* 在石匠坊上面按下 :kbd:`q` 键。
* 按下 :kbd:`a` 键选择 :guilabel:`Add new task` （新建任务），之后按下 :kbd:`d` 键来制作门。重复四次。
* :kbd:`a` 键新建任务， :kbd:`t`键制作桌子，重复两次。
* :kbd:`a` 键新建任务， 之后按下 :kbd:`c` 键制作椅子，重复两次。需要注意的是，在矮人要塞里，一个由石头制成的椅子是被称为 :guilabel:`throne` （宝座）的，由于使用不同的材料来制作相同的物品，最后物品的名称不一样，这种现象并不是只有石质椅子（宝座）才有的。如果你感兴趣的话，可以去看一下Wiki里的相关信息。

Wee! Look at all that lovely furniture queued up! Soon our mason will
turn up and start cutting blocks of stone into something more useful.
Unfortunately, someone is going to end up trying to haul all that
furniture up stairs to the furniture stockpile, and we can’t have that,
so lets make a big stockpile in the middle of this room.

耶！看看这些可爱的家具加入到任务队列中把。一会儿我们的石匠将会出现，并且开始切割石质物品，将之只作为更加有用的物品。但不幸的是，有些人最终会尝试将所有这些制成的家具搬到楼上的家具库存中，但是我们并不希望这件事发生。所以让我门在这个房间的中间规划一个大的库存区域吧。

While you’re at it, why not remove the furniture stockpile upstairs and
get all of that stuff out of the rain. Go up to the pile, hit :kbd:`p`
for pile, then :kbd:`x` and then hit :kbd:`Enter` at one corner of the
outdoors furniture stockpile, and then move the cursor to the other end
and hit :kbd:`Enter` again. Here’s my downstairs stockpile, complete with
some just-moved furniture and a mason hard at work!

让你正在规划的时候，为什么不把楼上的家具库存移除掉，并且让所有的这些物资免于风吹雨淋呢？向上一层，按下 :kbd:`p` 键打开网格选项，之后按下 :kbd:`x` 键，对着门外家具库存的一角按下回车键，将光标以到其对角再次按下回车键即可。这是我在楼下的家具库存，填满了刚刚被搬运的家具，还有一个正在辛苦工作的石匠！

.. image:: images/02-mason-use.png
   :align: center

Now it’s time to get those doors and beds into use, and when the tables
and chairs are built, we’ll use those too!

现在是时候让这些门和床来排上用场了，之后当桌子椅子什么的做完了，让我们也把这些玩意摆上去！

家，甜蜜的家
===============
You may have noticed that our dwarfs have been sleeping on the ground
when they get tired. This is really not much fun for them and we’d like
to give them a place to stay. With some beds built, lets set up a big
bedroom on the top floor so our dwarfs can get some shuteye.

你应该已经注意到了，如果他们累了的话，就会在地上直接睡觉。对于他们而言，这可不是一件有意思的事情，所以我们需要给予他们一个住处来休息。上一节我们已经做了一些床，所以让我们在最高层做一个巨大的速沙，让我们的矮人可以舒舒服服地睡上一觉。

* Head to the top floor, and dig out a small room at the end of the hallway.
* Hit :kbd:`b` and then :kbd:`b` again (for :guilabel:`Bed`).
* Move the cursor and place a bed in the corner (hit enter, select a
  bed and hit enter again).
* Spread the five beds around the room.
* Hit :kbd:`Esc` until we’re back at the main menu.
* Hit :kbd:`b` again, and :kbd:`d` for :guilabel:`Door`.
* Place doors across the room entrance.

* 将视图移动到最高层，然后在走廊的尽头挖出一个小房间。
* 按下 :kbd:`b` ，之后再次按下 :kbd:`b` （选择 :guilabel:`Bed` ）。
* 移动光标，在角落放置一张床（按下回车，之后选择一张床，再次按下回车即可）。
* 在房间里分散放置五张床。
* 按下 :kbd:`Esc` 键，直到我们回到了主菜单。
* 再次按下 :kbd:`b` 键，之后按下 :kbd:`d` ，放置 :guilabel:`Door` 。
* 在房间和入口的交界处放置门。

Here’s my room layout. The shadowy beds and door show that the beds and
doors aren’t placed yet. The green X shows me about to
place the next door.

这是我房间的布局。阴暗的床和门表明这些床和门还没有被放置，绿色的X号显示我们将会去放置的下一个门。

.. image:: images/02-beds-shadows.png
   :align: center

Pretty soon the room will be laid out and we’ll be ready to use it to
house our little workers. Here’s mine finished, doesn’t it look pretty?

过不了一会儿，门就会被放置，我们将准备把这件房间给我们的小工人们提供住宿。我的房间已经完成了，看起来是不是很棒？

.. image:: images/02-beds-built.png
   :align: center

But the dwarfs won’t use it as a bedroom yet. We have to specify what
the room is to be used for first. To do this:

但是我们的矮人们冰不会将这个房间当作卧室。所以首先我们必须指明那个房间是拿来给他们住的。如果要做到这一点的话：

* Hit :kbd:`q`.
* Move the cursor near one of the beds. It will start flashing green.
* On the right, you’ll see the option :kbd:`r` :guilabel:`Make Bedroom`.
* Hit :kbd:`r`.
* You will now see a flashing blue square. Here’s mine, as selected
  from the bottom-left bed.

* 按下 :kbd:`q` 。
* 将光标移动到一张床的附近。它会开始闪烁绿色的符号。
* 在右侧，你会看到选项 :kbd:`b` :guilabel:`Make Bedroom` （制作卧室）。
* 按下:kbd:`r` 。
* 你将会看到一个闪烁的绿色区域，这是我的显示范围，因为选择了靠近左下角的那个床。

.. image:: images/02-beds-room.png
   :align: center

We could hit enter now and set the room at this size, but that would
leave two beds out. So we need to make the room size bigger. With the
blue square flashing…

你现在可以按下回车，然后设置这个区域作为卧室，但是这样做的话就会有两张床在区域之外了。所以我们需要让卧室的面积大一点。在蓝色光标闪烁的时候……

* Hit :kbd:`=` and this will make the blue square bigger
  until it fills the whole room.

* 按下 :kbd:`=` 键，之后蓝色区域就会更大，再次按下直到蓝色区域覆盖整个房间。

You will note that the room won't "leak" beyond the boundaries of the
walls and doors. This is why we need doors, to prevent leaky rooms!
Makes sense? Right? Ok… moving on…

你会注意到，这片蓝色区域不会越过墙和门的边界，这就是我们需要门的原因了，就是为了防止在这时候出现房子“漏风”的现象！这合理吗？不是吗？好，继续下一步……

* Hit :kbd:`Enter` with the room set at max size, a new menu will appear on
  the right.

* 按下 :kbd:`Enter` 键，让这片空间处于最大的尺寸，右侧将会出现一个新的菜单。

This menu gives you options for the room. It will always appear when
you :kbd:`q` over the item you set a room’s use from. Note, you don’t
have to set every bed in the room as a bedroom (although the game will
let you do that). DF is smart enough to know that the room is a
bedroom, and all the beds in the room should be used.

这个菜单将会提供一些对该房间的选项。当你在和房间用途相关的物品上面按下 :kbd:`q` 键，这个选项载视会出现。注意了，你不必将所有的床设置为一个卧室（尽管游戏将会让你这样做）。矮人要塞的游戏机制足以聪明，它能理解这个房间是一个卧室，以及在这个房间内的所有床位都应该被使用。

On the new menu you want to:

在新的菜单里，这样做：

* Hit :kbd:`d` for :guilabel:`Dormitory`, this will turn the
  :guilabel:`(N)` to a :guilabel:`(Y)`.

* 按下:kbd:`d` :guilabel:`Dormitory` （宿舍），这将会把 :guilabel:`(N)` 切换为 :guilabel:`(Y)` 。

With the room set as a dormitory any dwarf without their own room will
use the beds in the dormitory to sleep in. When you have a military, a
:guilabel:`Barracks` is where dwarfs will spar and sleep when off duty.

宿舍已经设置完成，当矮人没有自己的房间，他将会用宿舍里的床位来睡觉。当你有了一个军队的时候，一个 :guilabel:`Barracks` （军营）将会是（小队）矮人们在下班时争论于睡觉的地方。

Well done! We have a shared bedroom for our shorties to sleep in!

干的漂亮！我们有了一间宿舍，让我们的矮人们有地方睡觉了！

In `the next chapter of the walkthrough <chapter03>`, we’ll build a fine dining
room, set up some more workshops, and start to build some proper living
quarters for our dwarves. I can’t wait!

在 `流程的下一章节 <chapter03>` ，我们将会建造一个餐厅，设置更多的工坊，然后去为我们的矮人们创造更多更好的生活区。我可等不及了！