.. _chapter05:

########################
工业革命
########################

You have come a long way, young jedi! Your dwarf herding skills are
strong! You now know the basics of building, production, trading and
managing your dwarves.

年轻的绝地武士呦，你已经走过了很长的路程了！你矮人放牧的技能真不错！现在你应该知道了建筑、生产、交易与矮人管理的基础内容了。

This is probably a good time to go read the tutorial on trading -
it ties in pretty well with a large industrial base.  Tutorials are
designed be skippable though, so if the thought of heavy industry
excites you, we can dive right in!

现在应该是一个不错的时机，让我们开始阅读关于交易的教程吧——交易与深厚的工业基础可是密不可分的。尽管教程是可以跳着阅读的，那么如果你已经迫不及待地想了解关于重工业的相关内容，那么你现在就可以阅读了！

更多产品！
================
Lets start with some new workshops you should get
to grips with. I won’t list the keys you need to use for every task,
you should be able to remember the basic stuff from earlier lessons, so
lets just get building these workshops around our four big production
rooms:

让我们以工坊作为开始吧——你可得把握住这些工坊。我不会仔细地罗列每个任务所需要做的关键点，而你也应该能记得住先前教程里的基础操作。所以让我们就在这四个大房间里建造这些工坊吧：

* Mechanic’s Workshop（机械工坊）
* Leather Works（皮革工坊）
* Farmer’s Workshop（农务工坊）
* Kitchen（厨房）
* Butchers（屠宰场）
* Metalsmith’s Forge（锻造工坊）

In addition, go to the Furnaces menu (from :kbd:`b` and then :kbd:`e` for
:guilabel:`Furnaces`) and build:

除此之外，来到熔炉菜单（从  :kbd:`b` 到  :kbd:`e` ，显示为 :guilabel:`Furnaces` （熔炉）），之后建造：

* Wood Furnace（木炭炉）
* Smelter（冶炼炉）

While those places get built lets look at expanding a bit. In the
middle of our four room area we have three stairs going down. Right now
they go nowhere, so lets go down about 5 levels. Remember, :kbd:`d` and then
:kbd:`i` for :guilabel:`Up/Down Stairs`, set to the level below, will get that
construction going.

在这些工坊都在建设中的时候，让我们将目光转向扩建。在我们四个房间的中间，我们有三个楼梯是向下的。而现在它们并没有接着往下延伸。所以让我们向下挖5层吧。记住了， :kbd:`d` 之后  :kbd:`l` ，找到 :guilabel:`Up/Down Stairs` （向上/向下楼梯），将区域划为下面的平面，将会让整个工程持续进行下去。

We also have room to expand between our production floor and our
entrance, so lets go up a level and put some storage there. I suggest
the easiest way to do this will be to :kbd:`b`, build a :kbd:`C`
constructed up-stair. So chose the space I highlight
below, and then follow along!

我们还需要扩展我们的房间，在入口层与生产层之间。所以就让我们向上一层，在那里放置一些库存。我个人觉得，实现这个操作最简单的方式就是按下 :kbd:`b` 后，建造一个 :kbd:`C` 向上的阶梯。所以选择我在下面展示的高亮区域，之后跟着照做就可以了。

.. image:: images/dftutorial79.png
   :align: center

Find the spot in the picture above, just below those down stairs.
找到上图所示的位置，就是那些向下的楼梯之下。

* Hit :kbd:`b`
* Hit :kbd:`C` (:kbd:`Shift`-:kbd:`c`)
* Hit :kbd:`u` for :guilabel:`Up Stair`.
* Chose some stone.
* Repeat to set three up stairs to be built.

* 按下 :kbd:`b`
* 按下 :kbd:`C` （ :kbd:`Shift` -  :kbd:`c` ）
* 按下 :kbd:`u` ，对应 :guilabel:`Up Stair` （向上的楼梯）
* 选择一些石头
* 重复设置三个应该建造的楼梯。

Once the job is done you’ll have some stairs like this:
如果这些工作完成了，那么你会有一些如下图所示的楼梯：

.. image:: images/dftutorial80.png
   :align: center

Now we want to connect this level to the one above. If you go up a
level you won’t see anything but dirt, but we know the stairs are
leading up to this level, so it’s a matter of constructing some, right?
WRONG! We want to designate some stairs, because our miners will carve
out some matching stairs! We only use :guilabel:`Construct` when we’ve got an
empty space to deal with.

现在我们需要连接这一层与上一层。如果你向上一层后，你会发现除了泥土其他没有任何东西。但是我们知道楼梯通向这一层，所以我们还是需要建造一些楼梯，对嘛？不对！我们需要去设计一些楼梯，因为我们的矿工将会雕刻出一些与之对应的楼梯！当已经有一些空间处理后，我们只需要使用 :guilabel:`Construct` 就可以了。

One challenge you’ll face is working out where on the floor above the
spot is you need to dig out. So try this technique:

你现在所面对的一个挑战，是确定楼上哪里需要挖掘的位置。所以让我们试试下面的步骤吧：

* Hit :kbd:`d`
* Hit :kbd:`i` for :guilabel:`Up/Down Stair`
  (we want a series of stairs almost back up to the surface).
* Put your X over the top stair in the line, like the screenshot

* 按下 :kbd:`d` 。
* 按下 :kbd:`i` ，对应 :guilabel:`Up/Down Stair` （向上/向下的楼梯）。（我们需要一些楼梯，让这些楼梯通向几乎到达地面的地方。）
* 将你的“X”十字标放在顶部的楼梯上，就像下面的截图一样。

.. image:: images/dftutorial81.png
   :align: center

* Now go up a level and at the above level, designate three stairs in a
  row (ie, hit enter, then hit down-arrow a couple of times, and then hit
  enter), the above level should look like this:

* 现在可以向上一层，来到楼上的位置，规划一排三个楼梯（ie，按下回车，之后按下方向键几次，之后按下回车键），上层的样子应该如下图所示：

.. image:: images/dftutorial82.png
   :align: center

You’ll note I wasn’t quick enough when I took this picture and two of
my three stairs are already dug out. Good dwarves!

你会注意到我截图的速度并不快，所以当我截图的时候，3个规划中的楼梯已经有2个完工了。矮人们干得好啊！

With the new stairs dug out (providing easy access to all the space
we’re going to create for our production dwarves) lets make a massive
space for them to stockpile goods. Here’s how much I dug out:

在新的楼梯完工的同时（为负责生产的矮人们提供更加方便的通路，通向我们将会开辟的空间），让我们制作一大片区域，让他们储存货物。下面就是我挖出来的空间大小：

.. image:: images/05-storage.png
   :align: center

In case you’re wondering, that’s about a 40×40 box with our first
stairs at the bottom. And one nice thing about digging in dirt (which
this layer is), is that it doesn’t leave any messy rock around to
clutter up our nice stockpiles!

更详细地说，这是一个大约40x40的一个方形区域，还有我们一开始规划的楼梯在底部。另外一件很棒的事情是，我们是在泥土层挖掘（也就是这层），并不会留下一堆杂乱的石头搞乱我们的库存区！

While that’s completing, lets start an important job, making our dining
room awesome! You see, dwarves love to spend time in an attractive
meeting hall. Right now mine is packed with loafers. They clearly need
a bit more room! A bit of digging will sort that:

当这片区域完工后，让我们启动一项十分重要的任务，那就是让我们的餐厅更加富丽堂皇！你会发现，矮人们很喜欢在集会厅里消磨时光。现在我的会议室里人满为患。他们确实希望能有一个更大的集会厅！一些挖掘的工程能以以下的方式进行：

.. image:: images/05-meeting1.png
   :align: center

Once the room is expanded, fit doors and some more tables and chairs.
Once that’s done you’ll need to do something important, and that’s
resize the room. As you may remember we set the room up from one of the
tables. But if you :guilabel:`q` over the dining room table now you’ll see it
doesn’t fill the space:

只要房间扩充完毕，那么就需要让这个房间拥有相适应的门、更多的桌子和椅子。这个干完后，你就需要做一些重要的事情，那就是重新规划房间的大小。你应该能记得我们是利用一个桌子来设置集会厅的。但是如果你在餐厅的桌子上按下 :guilabel:`q` 的话，你会发现他所在的空间并没有覆盖所有的空间。

.. image:: images/05-meeting2.png
   :align: center

The room will be more valuable and widely used if it’s set to be
bigger, to fill this space. Lets do that now:

如果这个房间设置为更大一点的话，那么它将会更加有价值，并且拥有十分广泛的用途。为了将其空间设置完成，咱们按照以下的方式进行就可以了：

* Hit :kbd:`q` and move the X over the table which is setting the room up…
* Hit :kbd:`r` for :guilabel:`Resize Room`.
* Using :kbd:`=` expand the room size till you fill the space:

* 按下 :kbd:`q` ，之后移动光标到设置房间的那个桌子……
* 按下 :kbd:`r` ，即 :guilabel:`Resize Room` （重新设置房间大小）。
* 使用 :kbd:`=` 键来扩展房间的尺寸，直到你能将所有空间全部填满。

.. image:: images/05-meeting3.png
   :align: center

* Hit :kbd:`Enter`

* 按下 :kbd:`Enter` 。

Job done! The room is resized and better. But we want to make it MUCH
better because I noticed a dwarf with a red down arrow flashing and
when I viewed his info it turned out he was pretty unimpressed with his
surroundings. Lets get to impressing him!

任务完成！这个房间更大更好哩。但是我们想要让这个房间变得更好，因为我注意到一个矮人的身上有红色向下的箭头闪烁。当我查看他的信息时，显示说他对他的环境不是很令人耳目一新。让我们做点东西，来让他留下点深刻印象吧！

How do we do that? Simple! We smooth the walls and then engrave them
with fine carvings all about our fortress! To do this:

我们该如何去做呢？很简单！我们可以将墙面变得光滑，之后在整个要塞的墙上雕刻精美的刻纹！你需要这样做：

* Hit :kbd:`d`.
* Hit :kbd:`s` for :guilabel:`Smooth Stone`.
* Select the entire dining room and walls using :kbd:`Enter`, move cursor,
  :kbd:`Enter`.

* 按下 :kbd:`d` 。
* 按下 :kbd:`s` ，对应 :guilabel:`Smooth Stone` （平滑石头）。
* 选择整个餐厅和餐厅的墙体，按下 :kbd:`Enter` ，移动光标，再次按下 :kbd:`Enter` 。

The room will now look all flashy like this (until you back out of the
menu, anyway):

整个房间将都会像如下图所示的那样闪烁（直到你返回菜单，按你胃）：

.. image:: images/05-meeting4.png
   :align: center

Any dwarf with the :guilabel:`Stone detailing` labour on will now set about
smoothing the walls and floors. The next step, once the space is
smooth, is to designate the room to be engraved using :kbd:`d`, :kbd:`e`.
Engraved walls make dwarves happy and increase the value of your
fortress. You can even look at them by using :kbd:`k` and hitting enter with
the engraving highlighted. Some walls have some quite amusing
engravings (randomly generated and based on the history of your dwarves
and your fortress), so it’s worth looking around and finding the good
ones. The better the quality of the engraving the more text there is to
read, so keep an eye out for the engravings with the metal-bars icon
next to them.

拥有 :guilabel:`Stone detailing` 职业的矮人将会去平滑墙面和地板。下一步，只要整个空间都被光滑过后，就可以去雕刻整个房间了——使用 :kbd:`d` 后  :kbd:`d` 即可。雕刻墙面将会让矮人们变得开心，同时会提升你的要塞的价值。你甚至可以通过按下 :kbd:`k` ，之后在高亮的雕刻处按下回车，有一些墙面会有些十分有趣的雕饰（随机生成，基于你的矮人和你的要塞的历史），所以朝周围看看然后找一些比较好的雕饰，还是挺有意思的。雕刻的质量越好，阅读的文本就越多，因此请留意旁边带有金属条图标的雕刻。

With the smoothing under way, lets get back to the piles. In that big
space upstairs you can pretty much set up a space for everything it’s
possible to make a pile for (except refuse, of course, which we want to
be outside). So go do that now. Here’s how mine is laid out.

在那边正在雕刻的时候，让咱们把视野拉回看，看看咱们的库存区。在楼上这么大的区域内，你就能够将所有的东西都塞到里面去，当然除了废料，我们要将这个扔到外面去。嗯那么现在咱们就开干吧，这里是我的库存区布局。

.. image:: images/05-all-stockpiles.png
   :align: center

Pretty complete huh? Well, almost, I’m sort of tempted to make a stone
pile and using custom settings have it as metal ores only. It would
make things a bit tidier, but on the other hand probably doesn’t gain
me that much efficiency right now. Be very careful with stone
stockpiles, hauling stone can consume a heap of your dwarves’ time,
which is a bit pointless.

十分完备了是吧？嗯，只能说几乎是了吧。我是有点想做一个石头库存区，之后利用自定义设置来让其只存储金属矿物。这会让库存区变得更加整洁，但是另一方面，现在的情况可能并不能让我们拥有更高的效率。对待这些石头库存可是要小心翼翼的，搬运一堆石头可是会很浪费矮人们的时间的，而且也没什么意义。

加州要塞旅馆！
===================
We’ve got some new workshops set up, we’ve got some great storage, and
we’ve dug down a few levels. Good stuff! Unfortunately, (or
fortunately?) you’ve probably had a bunch of immigrants arrive over the
course of the past couple of tutorials and they haven’t been assigned
anywhere to live. We also don’t have any space dug out for the
inevitable arrival of nobles, and these boys and girls are one set of
dwarves that need hard work to keep happy!

我们已经规划了几个新的工坊，也有了很大的库存区，我们还往下继续挖了几层。干得漂亮！但不幸的是（或者说，幸运的是？），在这几篇教程实践的过程中时，你也可能也有了很多到达的移民，但他们还没有地方住。我们也还没有挖出来一片空间，来专门迎接贵族们的到来。这些男孩女孩们是一群需要努力工作才能保持快乐的的矮人们！

Fortunately we’ve dug down a few levels and we have a lot of nice rock
down there. So lets go take some time to lay out some great bedrooms
for our dwarves. The majority of your rooms should be 2×2, but lets make
some space for nobles too.

幸运的是我们已经向下挖掘了几层，我们也拥有一些不错的岩石落在那里。所以为了让矮人们的住宿，我们来花点时间布局一些不错的卧室吧。你房间的尺寸大多数应该是在2x2，但是我们也应该给贵族们多留一些空间。

Nobles usually want two-to-four rooms. So lets make things easy and
give them all four rooms of size 3×3 or so, that should be good enough.
Here’s how I have planned out my bedroom level:

贵族们通常想住在2到4间房间中。所以让我们化繁为简，给他们准备4间3x3或者更大的屋子，这样的话应该是足够了。下面就是我对卧室的布局方案：

.. image:: images/05-big-bedrooms.png
   :align: center

A couple of points. Firstly, the whole right side of my new bedroom
level won’t be dug because it’s not connected to the stairs, etc. I’ll
connect it up later once we’ve got the left side done. Second, my
layout is pretty boring, go and check out some of the bedroom designs
on the wiki. I love the fractal pattern! Very efficient! Finally,
you’ll notice the big wide corridors for the main arterial routes.
dwarves need space to move around, remember!

这里需要指出一点，首先，我新的卧室层不会被挖掘出来，因为它们并没有连接到楼梯。只要我们将右侧布置好，我就会将它们连接起来。第二点，我的布局可以说是十分没劲的，你可以去Wiki上查看一些卧室的设计。但我喜欢分形图案！十分有效率！最终，你会注意到这个贯穿整个主要干道的巨大宽敞走廊。记住了，矮人们需要空间来四处走动！

Also, remember that you will need a lot of new doors and beds for our
beautiful hotel. While you’re at it, build a ton of tables and chairs
and at least a half dozen coffers, cabinets, armor stands, weapon
stands. You’ll find all those items under the masonry workshop menus.
Oh, and lets connect up the southern most stairs (the first ones we
built way back in tutorial 1 or 2) with this level as well. Construct
down stairs from the workshop floor and then designate Up/Down stairs
till we’re all connected up.

与此同时，记住你需要很多很多新的门和床铺，来装饰你美丽的大酒店。当你完成这项工作后，建造很多很多的桌子、椅子以及六七个保险柜、盔甲架、武器架。你会在石工坊的菜单里找到这些东西。哦，别忘了将西边大多数的楼梯连接起来（我们建造的第一个楼梯见教程1和2），就在这一层连起来。从工坊所在的那层建造向下的楼梯，之后设计上/下楼梯，直到所有的楼梯都连接起来。

While that is being built (oh, we’re going to find some gems too,
cool!) lets continue with-
当这些建筑完工后（对了，我们还要去找一些宝石），让我们翻开下一页……

矮人们和他们的怪脾气
================================
Oh dear! Something is going down in dwarf land!

不好了！有些坏事降临在矮人中间了！

    :guilabel:`Endok Oltarisos, Tanner, withdraws from society...`
    :guilabel:`Endok Oltarisos，制罐师，从社会中脱节了…… `


If you get a dwarf in a strange mood, find them using :kbd:`u`, looking for
their name, and then :kbd:`c`. You will see the dwarf flashing with a grey
exclamation mark (red is very bad, by the way). Follow this dwarf
closesly. This dwarf has got a strange mood and is off to claim a
workshop to start building some amazing object based on their whim and
fancy. We can’t control what they build, all we can do is hope they
build something cool and that they can find all the materials they want
for their fancy. If not, they go suicidal or homicidal. Oh dear! Lets
watch and see what happens. Of course, this event is random, so it
might not happen to you at this point in the game, but it will happen
sooner or later.

如果你发现矮人出现了奇怪的心情，那么通过按下 :kbd:`u` 键，寻找他的名字来找到他，然后按下 :kbd:`c` 。你会发现矮人闪烁着感叹号的标记（顺便一提，如果感叹号是红色的，那么情况就很糟糕了）。让我们近距离跟随这位矮人。这个矮人现在的心情很古怪，他动身去认领工坊，准备发挥奇思妙想，去建造叹为观止的精妙物件。我们无法控制他们想要的造物，而我们能做的，则是目视着他们制作出炫酷的物品，同时也需要准备能够满足其幻想的原材料。否则，他们就会疯掉，要么以头抢地，要么滥杀无辜。看哪，让我们继续观察接下来发生的事情吧。当然，这个事件是随机的，所以在你的游戏中现在可能不会发生，当然这种事情早晚会发生。

Right, my dwarf, Endok Oltarisos has rushed off to claim a leather
workshop. No surprise, he’s a tanner after all. Once he claims the
workshop you can :kbd:`q` and see the status of the workshop, and if you
wait, it will scroll through what items the dwarf is looking for. Endok
is looking for :guilabel:`stacked leather` and :guilabel:`skeletons`.
I’m not sure if I’ve got any. I’ll find out pretty quickly though,
the dwarf will either run off and start fetching stuff, or sit in
the workshop, seemingly doing nothing…

好的，我的矮人，Endok Oltarisos，已经冲到了一个皮革工坊。他是个皮革匠，所以这并不奇怪。只要他认领了一个工坊，你就可以按下 :kbd:`q` 键，来查看该工坊的状态。如果你有耐心地等一下的话，那么将会显示矮人所需要的材料。Endok需要 :guilabel:`stacked leather`  （大量皮革）和 :guilabel:`skeletons` （骨架）。我其实不太清楚这些材料我有多少，但我会尽快找齐这些东西的。矮人看起来要么就是四处乱跑，要么就是在拿东西，或者就是坐在工坊里，看似在罢工……

.. image:: images/dftutorial92.png
   :align: center

Well, the leather isn’t a problem, I just bought a ton from a trader,
and the bones, I think he’s grabbed some from the refuse pile.
Thankfully, my worries about the dwarf not getting stuff are put to
rest when I get this message:

嗯，皮革并不是个问题，我从商队那边买了一大堆皮革和骨头。我觉得他应该能去废弃物存储区那边拿一些。谢天谢地，我的担心从这条消息冒出来的时候便烟消云散了：


    :guilabel:`Endok Oltarisos has begun a mysterious construction!`

    :guilabel:`Endok Oltarisos开始了一项神秘的工程！`

Endok has begun a mysterious construction! Great! Now we just wait and
see what crazy object the dwarf produces. With luck it will be useful!

Endok开始了一项神秘的工程！太棒了！现在我们只需要等待，然后观察这个矮人在创作什么令人疯狂的物品。希望这个物件是特别实用的……

    :guilabel:`Endok Oltarisos, Tanner, has created Modonnokoi, a dog
    leather cap!`

        :guilabel:`Endok Oltarisos，皮革匠，制作了Modonnokoi，一顶狗皮帽！`

..or maybe not! Our dwarf has made a cap, a simple hat, out of dog
leather! Damn! On the plus side though the dwarf has become a legendary
tanner. If they gain skill from a mood it often leaves them
Legendary, which is pretty neat. With legendary skill I could use
Endok to make leather armour and it would be almost as good as metal armour.

也可能并不实用！我们的矮人制作了一顶帽子，一个简单的帽子，一个狗皮帽子！焯！在另一方面，这个矮人成为了一个传奇级的皮革匠。如果他们从古怪的心情中获得技能的话，他们经常能够成为传奇级，而这也确实不错。拥有了传奇级的技能，我们就可以让Endok来制作皮甲，而这些皮甲应该能媲美金属制的护甲。

Lets look at the item. If we hit :kbd:`l` we get a list of artefacts. With
only one artefact there’s no list, so we can go straight in to hitting
:kbd:`v` to :guilabel`View`. Behold! Triberiddle, the dog leather cap!

让我们来查看这个物品。如我我们按下 :kbd:`l` ，我们会得到一个神器列表。当只有一件神器的时候，这里并没有列表，所以我们直接按下 :kbd:`v` 即 :guilabel`View` （查看）。当当！Triberiddle，新鲜出炉的狗皮帽子！

.. image:: images/dftutorial95.png
   :align: center

A nice hat depicting when one of the trade caravan guards shot a
goblin. No one will wear it though, it’s an :wiki:`artifact`, and only
champions are important enough to grab artifacts from stockpiles.

一件漂亮的帽子，上面绘制了商队射杀了哥布林的场景。没有人会戴上它，因为它是一个 :wiki:`artifact` （神器）。只有捍卫者才有资格将它从储物区拿出来。

We were lucky this time with our moody dwarf. He was able to get
everything he needed to make his artifact. If he couldn’t find it he
would go quite crazy in the workshop, or if a suitable workshop isn’t
available, in his room. When you see the dwarf start to go crazy
(flashing down arrows and not moving from their workshop are a good
sign) it’s time to either assign the dwarf some war dogs (more on that
later) or to construct some walls and wall them in to their workshop,
or to lock the door on their quarters. At some point the dwarf will go
beserk and either get attacked by nearby military dwarves or war dogs,
or if locked inside, slowly starve to death.

对于这次处理古怪心情的矮人，我们算是比较幸运的。他能直接获得所有需要的原材料来制作他的神器。如果他无法找到足够的原材料，他会在工坊里发疯，或者说如果没有合适的工坊，他就会在自己的卧室里生闷气。当你观察到矮人开始发疯（闪烁的向下的箭头，同时也不会从他们的工坊里出来，这些都是判断的标志。），这时要么让矮人们调派一些战犬（等会儿可以调集更多），或者在工坊周围建造一些墙，将其围困在工坊中，或者将他所住的房间锁上门。在某个时刻，这个可怜的矮人将会狂怒，要么被周围的矮人军队或战犬攻击，要么被困在狭小的地方，慢慢地饿死。

If the crazy dwarf is ignored they will destroy stuff and attack
dwarves, probably killing a couple before they are put down, so watch
those moody stunties closely!

如果忽视发疯的矮人，那么他将毁坏器物，攻击矮人，在被放到之前顺便带走一两个矮人也是很常见的。所以要时刻照看着他们的心情！

In `the next chapter <chapter06>`, we'll cover traps - used to "mine" a
kind of iron ore the community calls "goblinite".  See you then!

在 `下一章节 <chapter06>` ， 我们将会探索陷阱——“开采”一些铁矿，或者社区玩家所称的，叫做“哥布林合金”的矿物。到时候再见啦。