.. _chapter03:

#################
从洞穴到住所
#################

In this tutorial we’ll set up some more living space for our dwarves as
well as more workshops and stockpiles. This will just be a short one as
I’m going to give you some tasks to do on your own at the end. Are you
up to the challenge?! Lets do it!

在这部分，我们将会为我们的矮人们建立更多的生活区域，以及更多的工坊与库存区。这一章节的内容稍短，所以物品将会在最后给你一些任务去完成。准备好迎接挑战了没？开始吧！

回顾时间
==========
We’ve covered quite a lot so far, and if you’ve played through, and
you’re still reading, you’re well on your way to being a master of
Dwarf Fortress. Lets look at what we’ve learned:

我们现在已经做了很多事情了，如果你跟着流程一直玩下去的话，你可是在征服矮人要塞这款游戏的路上越走越远了。让我们来回顾一下我们学到了什么内容：

* How to look around and work out what you’re looking at.
* How to dig out space and how to dig stairs.
* How to set up rooms and workshops.
* How to get some basic resource production and gathering going.

* 如何查看四周，并且能够知道你正在查看位置的详细信息。
* 如何挖出一片空间，以及如何挖出正确的楼梯。
* 如何设置房间和工坊。
* 如何进行一些基础的资源产出和资源收集。

One thing we haven’t covered is saving your game! Perhaps an
oversight?! Simply hit :kbd:`Esc` and then select :guilabel:`Save Game`.
But don’t do it now! :guilabel:`Save Game` assumes you want to exit
and it will take you back to the main menu. Whatever you do, **don't**
choose :guilabel:`Abandon the Fortress` - that ends the game!

而有一件事情我们还没有提到，那就是如何保存你的游戏！说的有点晚，我的疏忽。简单地按下 :kbd:`Esc` ，然后选择 :guilabel:`Save Game` （保存游戏）就可以了。但是现在不要这样做！ :guilabel:`Save Game` （保存游戏）选项会认为你想退出游戏，然后就将你带回游戏的主菜单里去了。无论你想做什么， **请不要** 选择 :guilabel:`Abandon the Fortress` （废弃要塞），那将会永远地结束你的游戏！

I should note that saves can get confusing - if you start to play with
a seasonal autsave, then save that, the names get longer and don't
always sort in a useful way.  When you come back
to DF and hit :guilabel:`Continue Game`” choosing the last save in the list
won’t necessarily result in your most recent game starting. Instead, you may
have to look in your ``<DF>/data/save/`` folder and see which folder has the
most recently modified files. Then you might rename or delete the older folders.

我可能注意到了，保存会导致一些人十分困惑——如果你在游戏的时候进行季节性的自动保存，那么游戏自动存档，存档的名字将会更长，而且并不总是通过便于利用的方式进行分类。当你回到矮人要塞中，按下 :guilabel:`Continue Game` （继续游戏）选择列表中最后一个存档，将会导致无法进入最近的存档进行游戏。取而代之的时，你可能需要浏览你的 ``<DF>/data/save/`` 文件夹，然后看一下哪个文件是最后被修改的。之后你就可以将旧的文件夹更名或者直接删掉就行。

If you choose the wrong save, there are two options:  you can save as
usual and return to the main menu, or force-close DF with the DFHack
command ``die``.  Enter it in the ingame console (:kbd:`Ctrl`-:kbd:`P`),
but be careful - this will quit without saving or checking again!

如果你选择了错误的存档，那么这里有两个解决方案：你可以像往常一样存档，然后回到主界面，或者使用DFHack里的 ``die`` 指令，来强制关闭矮人要塞游戏。将指令输入在游戏的控制台里（ :kbd:`Ctrl`-:kbd:`P` ），但是一定要小心——这样做将会直接退出游戏，而不会进行保存和再次检查。

Good luck! And lets continue…

祝好运！然后让我们继续正题……

让洞窟变成家
========================
Lets look at making this place a little more comfortable for our
dwarves. First up, they really need a nice place to eat. Just picking at
food on the floor isn’t much fun, dwarves want a beautiful hall in which
they can quaff beer and eat cat biscuits (yes, you can make biscuits
out of cats. Ugggh). Lets help them set one up.

为了咱们的小矮子们，咱们可以考虑考虑让这块地方更加舒适。第一，他们需要一个吃饭的好地方。直接从地上拿食物吃可没多少乐趣，矮人们还是很希望拥有一个漂亮的餐厅，他们可以痛饮啤酒，吃一些小猫饼干（确实，你可以用猫来制作饼干，噫）。让我们帮他们造个餐厅吧。

For now we’re going to set it up near our newly-dug space. I’m also
going to make a bunch of 2 by 2 rooms, which will serve as permanent
bedrooms for our dwarves. There's no such thing as a perfect design, so
lets just get the function right and worry about the perfect layout in
your next fortress.

现在，我们需要将餐厅布置在靠近我们刚刚挖掘出的空间旁。我们同样也也要挖出一些2x2的房间，让它们成为我们矮人永久的卧室。没有所谓的完美的设计，所以就让我们的卧室能够起作用即可，顺便思考下一个要塞会有什么样更好的布局。

This is how I set things up:

这是我自己的设计方案：

.. image:: images/03-dig.png
   :align: center

While it’s being dug, go add a bunch of beds to the carpenter’s shop,
as well as doors and tables and chairs at the masons. Report back in
when the rooms are dug and you’ve got a bunch of furniture and another
couple of tables and chairs.

当矿工过去开凿房间的时候，在木工坊里做一批床，然后在石匠坊里做一批门、桌子和椅子啥的。等到房间已经开凿完毕，获得了一批家具和其他一些桌椅后，回来报告一下。

…Ok, are you done? Good!

……OK，做完了？漂亮！

Now you need to go and place a bed in each room, a door on each
doorway, and tables and chairs in the dining room-t0-be. You should
know how to do all of this using :kbd:`b` for :guilabel:`Build`, then
:kbd:`d` for :guilabel:`Door`, :kbd:`t` for :guilabel:`Table`, and
:kbd:`c` for :guilabel:`Chair`... which in a Mason's workshop is
called a :guilabel:`Throne`.  Yep, the same object can have different
names depending on the material - another reason using the shortcuts
can be easier than scrolling.

现在你需要处罚，然后在每个房间内放置一张床，在每个门口放置一个门，然后在餐厅里放置一些桌子和椅子。你应该知道如何这些操作吧，按下 :kbd:`b` 键，即 :guilabel:`Build` （建造），之后 :kbd:`d` 对应 :guilabel:`Door` （门）， :kbd:`t` 对应 :guilabel:`Table` （桌子），最后 :kbd:`c` 对应 :guilabel:`Chair` （椅子）……当然在石工坊里椅子被称作 :guilabel:`Throne` （宝座）。是的，一个相同的物品可以有不同的名字，且取决于其材料——另一个使用快捷键的原因，则是相比于滚动查找物品而言，你可以更轻松地找到物品。

Here are my rooms partly completed. If you look closely you can see a
slacker dwarf having a nap.

这就是我的房间，已经完成一部分了。如果你仔看的话，你可以看到一个爱睡懒觉的矮人正在休憩。

.. image:: images/03-beds-layout.png
   :align: center

Can spot my chairs, tables, beds and doors? Pretty aren’t they!? You
may have noticed that bits of your local map are flashing. Don’t worry,
this just indicates that objects are sharing the same space with other
objects, but yes, all that stone does make things look messy. If you
have a tidy-fetish, GIVE IT UP NOW! Dwarf Fortress forts often look
messy with stone and junk strewn everywhere. You can, however, do a few
things to fix that, but we’ll worry about that some other time.
Meanwhile, we need to make ourselves a dinning room!

我能不能查看我的椅子，桌子，床和门？怎么不可以呢！？你应该已经注意到了，你的地图有些地方是闪烁的。不用担心，这只是在提醒有些物品和其他物品共享同一个位置，但是确实，所有散落在地上的石头都让整个要塞看起来乱糟糟的。如果你有洁癖的话，现在就放弃吧！矮人要塞这款游戏的画面经常是乱糟糟的，石头、垃圾，到处都是。但是你可以做一些事情来解决这个问题，而现在还不是讨论这个问题的时候。于此同时，我们需要为我们的矮人们制作一个餐厅！

What’s that? Oh bugger!
=======================
Just got a message which reminded me that I’ve forgotten to do something...

刚刚收到了一条消息，提醒我已经忘记了什么事情……

    | :guilabel:`The Outpost Liason from Vushuvash has arrived.`
    | :guilabel:`A caravan from Vushuvash has arrived.`
    | :guilabel:`Their wagons have bypassed your inaccessible site.`
    | :guilabel:`The merchants need a trade depot to unload their goods.`

    | （来自Vushuvash的前哨联络官已经到达。
    | 来自Vushuvash的车队已经到达。
    | 它们的车队已经从你不可到达的地点旁经过了。
    | 商人们需要一个交易站来卸货。）

Damn! A trade caravan arrived but couldn’t make it to our fortress
because, 1: it is inaccessible, 2: we don’t have a trade depot. This
may have happened to you already, if so, don’t worry about it much,
we’ll get that problem fixed soon. Trade caravans come by fairly
regularly and represent other civilisations wanting to trade their
valuables for yours. They are important, and handy, but we’ll discuss
them later some time.

惊了！一个交易商队已经到达，但是它们没法到达你的要塞，这是因为：首先，他们的车队没法到达；其次，我们还没有卸货点。你可能早就收到这条提示了。如果是这样的话，那么先不用太担心，我们接下来将会解决这个问题。交易车队会有规律地到达要塞，然后代表其他文明的需要，来和你交易有价值的商品。他们可是十分重要的，同样也十分便利。但是这些内容我们在之后的一段时间再谈。

Oh, you may get various windows pop up from the visiting trade liaison.
Just :kbd:`Esc` back out of them for now.

呃，你可能看到很多窗口弹出来，这些窗口和拜访交易的联络相关。我们只需按下 :kbd:`Esc` 键忽略就行。

回到吃饭的地方去！
===============================
By now you should have the dining room furniture set up, so it’s time
to let the dwarves know it’s the official dining room of the fortress.
We do that in much the same way we set up bedrooms:

现在你应该有一个布置完家具的餐厅了，所以现在是时候让这些矮人们知道这是要塞里的餐厅啦。我们需要做的事情和设置卧室的步骤相类似：

* Hit :kbd:`q` and move the cursor over any one of the tables (and for good
  fun, move it over a chair and and read what sort of rooms chairs set up).
* When over a table, hit :kbd:`r`, and again you should see a flashing blue
  box which doesn’t quite fill the room.
* Use :kbd:`=` to expand the room to fill the dining room space.
* Hit :kbd:`Enter`.
* Now hit :kbd:`h` to turn the dining room into a meeting hall as well.
  The :guilabel:`(N)` on the menu will become a :guilabel:`(N)`.
* :kbd:`Esc` back out to resume the game. All tables and chairs in the
  entire space will now be used.

* 按下 :kbd:`q` 然后将光标移动到一个桌子上（如果感兴趣的话，将它移动到椅子上，然后阅读一下有椅子的房间可以被设置为什么类型）。
* 光标现在在桌子上，那么就按下 :kbd:`r` 键，再次按下吼你会看到一个闪烁的蓝色方框，而且并没有填充满整个房间。
* 按下 :kbd:`=` 键来将这个房间的所有空间设置为餐厅。
* 按下 :kbd:`Enter` 键。
* 同样，现在按下 :kbd:`h` 将这个餐厅设置为集会厅。现在在菜单上的 :guilabel:`(N)` 将会变成 :guilabel:`(N)` 。
* 按 :kbd:`Esc` 键，让游戏继续运行。在这片区域内的所有的桌子和椅子现在都会被使用了。

Well done! A dining room and meeting hall is now set up! Without a
meeting space immigrant dwarves get confused and don’t know where to go
when they arrive, milling about at the edge of the map. A meeting space
seems to send out invisible mind-control rays and any newly-arrived
dwarves will immediately home in on it and into your fortress. Handy!

干得漂亮！一个餐厅和集会厅现在就已经准备就绪了！如果没有集会厅的话，外来的矮人们将会十分迷惑，然后他们在抵达后也不知道首先到达何处，在地图的边缘乱逛。一个集会厅就仿佛会发射精神控制光线似的，新来的矮人们将会直接到达集会厅，然后顺其自然地融入你的要塞。这真的太方便了！

For amusement, go back up stairs and find the wagon we arrived with. I
bet you it has a few dwarves hanging around it. Know why? Because by
default your wagon is your first meeting area!

感兴趣的话，可以上楼，然后找到我们到达时的那个棚车那里。我拍着胸脯说，那里还会有几个矮人在这片区域闲逛。知道为什么吗？因为默认的话你的棚车就是你的第一个集会区域！

If you press :kbd:`F1`, the map will move to the wagon - because it's
also your first hotkey location.  The :kbd:`H` hotkeys menu allows
you to scroll through the hotkeys, name each of them, and set one
to zoom to your current location - which saves time once you spread
out vertically.  Try setting :kbd:`F2` to zoom to our big underground
rooms now.

如果按下 :kbd:`F1` ，地图将会将视角转到棚车——因为这同样是你的第一个热键所指定的位置。 :kbd:`H` 热键菜单允许你检视你所设置的热键，给每个热键命名，然后设置热键来将视图切换到你现在的地点——一旦之后的要塞在垂直方向上急剧发展，这些快捷键将会节省很多时间。现在就尝试设置 :kbd:`F2` 键来设置为地下集会厅的快捷键吧。

But the wagon is outside and a long way from the fort and we’d much
prefer our dwarves to be safe and sound within our walls. We should
probably remove that temptation to stand outside and get killed by
carp, elephants, monkeys, unicorns, skeletal whales, zombies, giant
eagles, deer, goblins, etc, so lets remove the wagon. Hit :kbd:`q` and move
the X over to the wagon, then hit :kbd:`x` and the wagon will be
:guilabel:`Slated for removal`. This should free up a three logs
(which the wagon is made up)
and will prompt any lazy, slacker dwarves to head back inside!

话说回来，外面的棚车到要塞的距离可是挺长的，我们还是更倾向于让矮人们安然无恙地呆在我们的铁壁之中。我们需要拒绝呆在外面的诱惑，就比如我们可能会被鲤鱼、大象、猴子、独角兽、骨鲸鱼、僵尸、巨鹰、麋鹿、哥布林等生物杀死。所以就让我们移除掉棚车吧。按下 :kbd:`q` 键，然后将光标移动到棚车上，之后按下 :kbd:`x` 键，之后棚车就会被 :guilabel:`Slated for removal` （计划移除）。 这将会产生3个原木材料（也就是棚车的原材料）。同时，这也将会让一群懒散的矮人们回到要塞内部。

每个人都有自己的房间！
=============================
Dwarves love to have their own room, much preferring it to sharing a
dormitory (which they will do  by default otherwise), and conveniently
we have set up a number of little rooms. Lets declare that they're
bedrooms, so our dwarves will come and claim one:

相比于都住在一个宿舍里面（如果没有单人间的话），矮人们更喜欢每人都有自己的房间。为了进行这部分的内容，我们已经挖出了几个小房间。让我们来公布一下，这就是他们的卧室，因此我们的矮人将会到达并认领其中的一个。

* Go to your empty bedrooms, hit :kbd:`q`.
* Chose a bedroom, moving the X over a bed until it’s flashing.
* Hit :kbd:`r`. The blue selection area probably fills the room.
* Hit :kbd:`Enter`.

* 来到你的空卧室，按下 :kbd:`q` 。
* 选择一一间卧室，将光标移动到一个床上，此时光标应该会闪烁起来。
* 按下 :kbd:`r` 键。蓝色的选择区域将会填满整个空间。
* 按下 :kbd:`Enter` 键。

You can assign the bedroom to a specific dwarf, but unless you want to
play favorites there's no need - if a dwarf wants a nap and doesn't
have a bedroom, they'll claim one of the unowned ones.  Once that
happens, the bedroom status will now look something like this:

你可以为一个特定的矮人指定一个特定的房间，但是除非你十分偏袒某个矮人，一般这是没啥必要的。如果有矮人想要打个盹，但是没有卧室，他们将会自己认领一间没有被占用的卧室。一旦这个情况发生吼，这个卧室的状态将会发生变化，就像下面的图所展示的那样：

.. image:: images/03-beds-dug.png
   :align: center

If you get confused about a bedroom (or any room’s status), just hit
:kbd:`q` again and move around over each object. Of interest is the fact
that you can assign one room to have multiple uses, for example, put a
bed and table in the same room and specify that the room is both a
bedroom and a dining room, but doing so reduces the overall quality of
both rooms. Don’t bother unless you have some clever reason to.

如果你对一个卧室（或是其他房间的状态）感到迷惑的话，那么就再次按下 :kbd:`q` 键，然后检查每个物体；一个有趣的事实是，你可以将同一个房间设置为不同的用处。比如说，将一张床和一张桌子放在同一间屋子里，之后你就可以将这个房间同时设置为卧室和餐厅，但是从总体而言，这样做的话将会降低房间的两个功能的品质。不要麻烦矮人们，除非你自己有明确且聪明的理由。

While you’ve got your cursor up, move it over the door and look at
those options. You can lock doors and you can also make them impassable
to pets. Don’t bother with that for now, just have a look around.

当你将你的光标移动下，将它移动岛门上，然后检查这些设定。你可以锁上这个门，然后你同样也可以让他们与宠物相隔离。但是现在的话不要用这个功能来戏弄矮人们，你只需要随便看看就成。

工坊之乐
=============
Lets set up more workshops. First, put another mason's workshop in with
the other one. It will be handy in the long run, I am sure. Set
up a couple of carpenter's workshops in the room next door. While
you’re at it, set up a big wood pile (:kbd:`p`, :kbd:`w`). The next thing to do
is to set up some more  piles. How about we go through and make a
:guilabel:`Finished Goods` pile? Build it using :kbd:`p`, :kbd:`g`.
Lets keep making stockpiles in those rooms. A few squares for cloth (:kbd:`h`),
leather, (:kbd:`l`), and bars/blocks (:kbd:`b`) makes sense. Where one pile
starts and another stops can get confusing, but give it your best shot.

让我们建造更多的工坊吧。首先，在另一个石工坊的旁边再放一间石工坊。在之后很长的时间里这将十分方便，我可以十分确定地说。在对门的那个房间里设置几个木工坊。安排完后，设置一个巨大的木材库存区（ :kbd:`p` ， :kbd:`w` ）。下一件需要做的事情是设置更多的库存区。检查一下房间，做一个 :guilabel:`Finished Goods` （已完成货物）的库存区怎么样？用 :kbd:`p` 键和 :kbd:`g` 键就可以建造了。让我们在这些房间里继续增添库存区：几个格子放衣服( :kbd:`h` )，几个格子放皮革( :kbd:`l` )，一点空间放锭与方块（ :kbd:`b` ），这个样子真不错。这里开始放一堆，那里有停下来了，这可能会让你觉得困惑，但是请尽你最大的努力来实现。

Finally, lets also add a Craftsdwarf’s Workshop to our room. Use
:kbd:`b`, :kbd:`w`, :kbd:`r`, chose the materials and place the workshop.
These are particularly handy and profitable workshops, but more on them later!

最终，让我们在房间里建造一个手工艺坊吧。使用 :kbd:`b`, :kbd:`w`, :kbd:`r`,键就可以了，选择一些材料之后将之放在我们的房间里。这个工坊可是十分方便而且十分赚钱的工坊，但是我们稍后再详细介绍这个工坊！

This is how mine looks:

这是我房间的布局规划：

.. image:: images/03-workshop.png
   :align: center

That’s all for now, except before I go I have some homework for
you. But don’t worry, this is the fun kind of homework. See if you can
complete these tasks before we meet again:

这就是现在的所有内容了，除了我走之前给你留下的一点小作业。但是不用担心，这种作业可是十分有趣的。来看看你能不能在我们再次相遇之前，把下面的任务做完呢？

#. Destroy the carpenter’s workshop upstairs.
#. Designate another wide area of trees to harvest. If the seasons have
   changed to autumn by now you’ll notice all the trees a pretty gold
   colour. Admire them before you saw them down.
#. Designate a bunch of plants to be harvested.
#. Make a lot more beer and a lot more barrels, or set up ``workflow`` for
   some of the furniture you'll need later (beds, doors, tables, chairs).
#. Make some :guilabel:`bins` in the carpenters shop - around 15 should do.
   You’ll have to scroll to find them in the carpenter :kbd:`a`
   :guilabel:`Add new task` list, or use :kbd:`a`, :kbd:`n`.
   Bins are what everything that isn’t food or booze are stored in.
#. Go to the mason’s workshop and set it to build blocks on repeat
   (:kbd:`q`, :kbd:`a`, :kbd:`b`, :kbd:`r`). Do this only if you’ve already
   managed to make a LOT of bins or your Bar/Block pile will get filled
   quickly!
#. See if you can make some stone crafts. You will find them under the
   :kbd:`g` (:guilabel:`rock`) sub-menu from the :guilabel:`Add new task`
   menu on the craftsdwarf’s workshop. Make sure you have lots of bins for
   your crafts to go in though, and a big finished goods stockpile too!

#. 拆掉楼上的木工坊。
#. 指定另一片树木繁茂的区域来进行砍伐。如果季节已经到达秋季的话，你应该可以注意到所有的树叶都会镀上一层金黄色。在将它们锯下来前，欣赏一下吧。
#. 指定一片植物来收获。
#. 做一大批酒和更多的酒桶，或者对一些即将用到的家具设置一个 ``工作流`` 来进行制作（床、门、桌子、椅子）。
#. 在木工坊里制作一些 :guilabel:`bins` （箱子）——大概15个就足够了。你会在木工坊的 :kbd:`a` :guilabel:`Add new task` （新建任务）列表里面找到它们，或者直接按下 :kbd:`a` ， :kbd:`n` 也行。
#. 来到石工坊，然后设置它来循环制作一批建筑方块 (:kbd:`q`, :kbd:`a`, :kbd:`b`, :kbd:`r`) 。做这个任务之前，你需要确保你已经成功地做出了一大批箱子，否则的话你的锭与块库存区将会很快填满。
#. 让我们来看看你能不能做一些石头工艺。你将会在手工艺坊的 :guilabel:`Add new task` （新建任务）菜单里的 :kbd:`g` （ :guilabel:`rock` （岩石））子菜单中找到。和上面一条一样，请确保你已经制作了很多的箱子来承载这些工艺品，同样也要指定一大片已完成货物的库存区！

Good luck! And see you soon, in `the next chapter! <chapter04>`

祝你好运！让我们 `下一章节再见 <chapter04>` ！